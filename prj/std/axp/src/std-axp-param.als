# ----------------------------------------------------------------------------
# - std-axp-param                                                            -
# - afnix:std:axp parameters definition module                               -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project section                                                          -
# ----------------------------------------------------------------------------

# system revision
const AFNIX:STD:AXP:MODULE-RMAJ 3
const AFNIX:STD:AXP:MODULE-RMIN 8
const AFNIX:STD:AXP:MODULE-PTCH 0

# default application info
const AFNIX:STD:AXP:MODULE-INFO "afnix xml processor"
# the copyright holder
const AFNIX:STD:AXP:MODULE-COPY "© 1999-2023 Amaury Darsch"

# ----------------------------------------------------------------------------
# - default section                                                          -
# ----------------------------------------------------------------------------

# default output file flag
const AFNIX:STD:AXP:SYSTEM-OFLG false
# default output file
const AFNIX:STD:AXP:SYSTEM-ONAM "axp.txt"

# ----------------------------------------------------------------------------
# - session section                                                          -
# ----------------------------------------------------------------------------

# the default output flag
trans afnix:std:axp:system-oflg AFNIX:STD:AXP:SYSTEM-OFLG
# the default output name
trans afnix:std:axp:system-onam AFNIX:STD:AXP:SYSTEM-ONAM
