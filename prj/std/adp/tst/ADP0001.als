# ---------------------------------------------------------------------------
# - ADP0001.als                                                             -
# - afnix:std:adp test module                                               -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   adp utilities test module
# @author amaury darsch

# load the project
interp:load "std-adp-prjld"

# unload the project
interp:load "std-adp-punld"
