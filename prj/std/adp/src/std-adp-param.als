# ----------------------------------------------------------------------------
# - std-adp-param                                                            -
# - afnix:std:adp parameters definition module                               -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project section                                                          -
# ----------------------------------------------------------------------------

# system revision
const AFNIX:STD:ADP:MODULE-RMAJ 3
const AFNIX:STD:ADP:MODULE-RMIN 8
const AFNIX:STD:ADP:MODULE-PTCH 0

# default application info
const AFNIX:STD:ADP:MODULE-INFO "afnix documentation processor"
# the copyright holder
const AFNIX:STD:ADP:MODULE-COPY "© 1999-2023 Amaury Darsch"

# ----------------------------------------------------------------------------
# - default section                                                          -
# ----------------------------------------------------------------------------

# the adp writing mode
const AFNIX:STD:ADP:WRITER-MODE (enum XHT MAN TEX)
# default output file flag
const AFNIX:STD:ADP:SYSTEM-OFLG false
# default output file
const AFNIX:STD:ADP:SYSTEM-ONAM "adp.txt"

# ----------------------------------------------------------------------------
# - xhtml writer section                                                     -
# ----------------------------------------------------------------------------

# the default style file
const AFNIX:STD:ADP:SYSTEM-XCSS "std-us-style.css"
# the default image file
const AFNIX:STD:ADP:SYSTEM-XIMG "std-us-afnix.png"
# the default description
const AFNIX:STD:ADP:SYSTEM-XDSC "AFNIX Writing System"
# the no header flag
const AFNIX:STD:ADP:SYSTEM-NOHD false
# the no body div flag
const AFNIX:STD:ADP:SYSTEM-NOBD false

# ----------------------------------------------------------------------------
# - nroff writer section                                                     -
# ----------------------------------------------------------------------------

# the default writer source
const AFNIX:STD:ADP:SYSTEM-XSRC "AFNIX"

# ----------------------------------------------------------------------------
# - session section                                                          -
# ----------------------------------------------------------------------------

# the default xhtml style file
trans afnix:std:adp:system-xcss AFNIX:STD:ADP:SYSTEM-XCSS
# the default xhtml image file
trans afnix:std:adp:system-ximg AFNIX:STD:ADP:SYSTEM-XIMG
# the default description
trans afnix:std:adp:system-xdsc AFNIX:STD:ADP:SYSTEM-XDSC
# the default no header flag
trans afnix:std:adp:system-nohd AFNIX:STD:ADP:SYSTEM-NOHD
# the default no body div flag
trans afnix:std:adp:system-nobd AFNIX:STD:ADP:SYSTEM-NOBD
# the default nroff writer source
trans afnix:std:adp:system-xsrc AFNIX:STD:ADP:SYSTEM-XSRC
# the default output flag
trans afnix:std:adp:system-oflg AFNIX:STD:ADP:SYSTEM-OFLG
# the default output name
trans afnix:std:adp:system-onam AFNIX:STD:ADP:SYSTEM-ONAM
