# ----------------------------------------------------------------------------
# - std-tls-utils                                                            -
# - afnix:std:tls utility functions unit                                     -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - copyright section                                                        -
# ----------------------------------------------------------------------------

# @return a formated revision string
const afnix:std:tls:get-revision-string nil {
  const rmaj AFNIX:STD:TLS:MODULE-RMAJ
  const rmin AFNIX:STD:TLS:MODULE-RMIN
  const ptch AFNIX:STD:TLS:MODULE-PTCH
  afnix:std:acl:get-revision-number rmaj rmin ptch
}

# @return the copyright message
const afnix:std:tls:get-copyright-message nil {
  # format the copyright message
  + (+ AFNIX:STD:TLS:MODULE-INFO ", ") AFNIX:STD:TLS:MODULE-COPY
}

# @return the full system version
const afnix:std:tls:get-revision-message nil {
  # get the revision info
  const mrev (afnix:std:tls:get-revision-string)
  # format the revision message
  const mesg (+ "revision " mrev)
  # add system info
  mesg:+= (+ ", afnix " interp:version) 
  mesg:+= (+ ", "       interp:os-name)
  # here it is
  eval mesg
}

# ----------------------------------------------------------------------------
# - plist section                                                            -
# ----------------------------------------------------------------------------

# dump a property list to the error stream for debug
# @param plst the property list to dump
const afnix:std:tls:write-error-plist (plst) {
  # convert the plist to a print table
  const ptbl (plst:to-print-table true)
  # write to the interpreter error stream
  ptbl:format (interp:get-error-stream)
}

# ----------------------------------------------------------------------------
# - i/o section                                                              -
# ----------------------------------------------------------------------------

# get a credential from the terminal
const afnix:std:tls:get-credential nil {
  # duplicate an interpreter for reading
  const di (interp:dup)
  # read the credential
  di:read-credential "credential: "
}
