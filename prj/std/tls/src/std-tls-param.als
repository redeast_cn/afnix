# ----------------------------------------------------------------------------
# - std-tls-param                                                            -
# - afnix:std:tls parameters definition unit                                 -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project section                                                          -
# ----------------------------------------------------------------------------

# system revision
const AFNIX:STD:TLS:MODULE-RMAJ  3
const AFNIX:STD:TLS:MODULE-RMIN  8
const AFNIX:STD:TLS:MODULE-PTCH  0

# the application name
const AFNIX:STD:TLS:MODULE-NAME  "tls"
# the application title
const AFNIX:STD:TLS:MODULE-INFO "afnix tls command layer"
# the copyright holder
const AFNIX:STD:TLS:MODULE-COPY "© 1999-2023 by Amaury Darsch"

# ----------------------------------------------------------------------------
# - default section                                                          -
# ----------------------------------------------------------------------------

# the server default host/port
const AFNIX:STD:TLS:SERVER-HOST  "localhost"
const AFNIX:STD:TLS:SERVER-PORT  4433
