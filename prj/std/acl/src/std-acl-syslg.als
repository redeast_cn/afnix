# ----------------------------------------------------------------------------
# - std-acl-syslg                                                            -
# - afnix:std:acl system logger unit                                         -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - global section                                                           -
# ----------------------------------------------------------------------------

# the system logger object
const afnix:std:acl:syslg (class)

# ----------------------------------------------------------------------------
# - initial section                                                          -
# ----------------------------------------------------------------------------

# preset the logger class
# @param lmod the logger mode
trans afnix:std:acl:syslg:preset (lmod) {
  # bind the logger object
  const this:super (afnix:sio:Logtee (interp:get-output-stream))
  # set the logger tee mode
  this:super:set-tee lmod
}

# ----------------------------------------------------------------------------
# - methods section                                                          -
# ----------------------------------------------------------------------------

# @return the super object
trans afnix:std:acl:syslg:to-super nil (eval this:super)

# set the logger debug mode
# @param dmod the the debug mode
trans afnix:std:acl:syslg:set-debug-mode (dmod) (this:super:set-debug dmod)

# set the logger tee mode
# @param tmod the the tee mode
trans afnix:std:acl:syslg:set-tee-mode (tmod) (this:super:set-tee tmod)

# set the logger file
# @paran lnam the logger file name
trans afnix:std:acl:syslg:set-logger-name (lnam) {
  try {
    # get the directory path
    const path (afnix:sio:get-base-path lnam)
    # eventually create the directory
    afnix:sio:mhdir path
    # bind the logger file name
    this:super:set-output-stream lnam
  } (errorln "error: cannot create log file " lnam)
}

# log a message with several arguments by level
# @param mlvl the message level to log
# @param args the arguments to log
trans afnix:std:acl:syslg:lvlog (mlvl args) {
  # compute message
  trans mesg (String)
  for (m) (args) {
    if (not (mesg:nil-p)) (mesg:+= " ")
    if (not (nil-p m)) (mesg:+= m)
  }
  # log the message
  this:add mlvl mesg
}

# log a message with several arguments
# @param args the arguments to log
trans afnix:std:acl:syslg:mvlog (args) {
  # compute message
  trans mesg (String)
  for (m) (args) {
    if (not (mesg:nil-p)) (mesg:+= " ")
    if (not (nil-p m)) (mesg:+= m)
  }
  # log the message
  this:add Logger:INFO mesg
}

# log an error message
# @param args the arguments to log
trans afnix:std:acl:syslg:evlog (args) {
  # compute message
  trans mesg (String)
  for (m) (args) {
    if (not (mesg:nil-p)) (mesg:+= " ")
    if (not (nil-p m)) (mesg:+= m)
  }
  # log the message
  this:add Logger:ERROR mesg
}

# log a debug message with several arguments
# @param args the arguments to log
trans afnix:std:acl:syslg:dvlog (args) {
  # compute message
  trans mesg (String)
  for (m) (args) {
    if (not (mesg:nil-p)) (mesg:+= " ")
    if (not (nil-p m)) (mesg:+= (m:to-string))
  }
  # log the message
  this:add Logger:DEBUG mesg
}

# log a parameter list by prefix
# @param pfix the message prefix
# @param plst the property list to log
trans afnix:std:acl:syslg:pllog (pfix plst) {
  # get the list length
  const llen (plst:length)
  # loop in the list
  loop (trans i 0) (< i llen) (i:++) {
    # get the property by index
    trans prop (plst:get i)
    # get the property name and value
    trans name (prop:get-name)
    trans pval (prop:get-value)
    # prepare the message
    trans mesg (pfix:clone)
    mesg:+= '['
    mesg:+= name
    mesg:+= ']'
    mesg:+= pval
    # log the message
    this:add Logger:DEBUG mesg
  }
}

# show the log contents
trans afnix:std:acl:syslg:show-logs nil {
  # get the logger length
  const len (this:length)
  # loop in the messages
  loop (trans i 0) (< i len) (i:++) {
    println (this:get-full-message i)
  }
}
