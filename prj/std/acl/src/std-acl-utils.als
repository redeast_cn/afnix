# ----------------------------------------------------------------------------
# - std-acl-utils                                                            -
# - afnix:std:acl utility functions unit                                     -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - type section                                                             -
# ----------------------------------------------------------------------------

# map an object representation
# @param value the value to map
const afnix:std:acl:to-repr (value) (
  if (object-p value) (value:repr) "nil"
)

# check that a string is valid
# @param value the value to check
const afnix:std:acl:object-string-p (value) {
  # check for nil
  if (nil-p value) (return false)
  # check for a string
  if (not (string-p value)) (return false)
  # check string content
  not (value:nil-p)
}

# map a literal to a string
# @param value the literal to map
const afnix:std:acl:to-string (value) (
  if (literal-p value) (value:to-string) "nil"
)

# map an argument to a literal
# @param value the value to map
const afnix:std:acl:to-literal (value) (
  if (literal-p value) (value:to-literal) "nil"
)

# map an argument to an integer
# @param value the value to map
const afnix:std:acl:to-integer (value) (
  if (integer-p value) value (Integer value)
)

# map an argument to a real
# @param value the value to map
const afnix:std:acl:to-real (value) (
  if (real-p value) value (Real value)
)

# map an argument to a complex
# @param value the value to map
const afnix:std:acl:to-complex (value) (
  if (complex-p value) value (Complex value)
)

# ----------------------------------------------------------------------------
# - copyright section                                                        -
# ----------------------------------------------------------------------------

# format a revision string
# @param rmaj the major number
# @param rmin the minor number
# @param ptch the patch number
const afnix:std:acl:get-revision-number (rmaj rmin ptch) {
  # format the number
  const mj (afnix:std:acl:to-string rmaj)
  const mn (afnix:std:acl:to-string rmin)
  const pt (afnix:std:acl:to-string ptch)  
  # build revision
  + (+ (+ (+ mj '.') mn) '.') pt
}

# ----------------------------------------------------------------------------
# - format section                                                           -
# ----------------------------------------------------------------------------

# @return a default print table style
const afnix:std:acl:get-default-style nil {
  const sdef (Style)
  sdef:set-scientific-notation true
  sdef:set-number-precision    5
  eval sdef
}

# convert a plist to a printtable
# @param plst the property list to convert
const afnix:std:acl:plst-to-ptbl (plst) {
  # convert to a print table
  const ptbl (plst:to-print-table true)
  # create a truncate style for column 2
  const cstl (Style)
  cstl:set-truncate 16 16
  # attach the style
  ptbl:set-style 2 cstl
  # here is the table
  eval ptbl
}

# write a property list to the output stream
# @param plst the property list to write
const afnix:std:acl:write-output-plist (plst) {
  # convert the plist to a print table
  const ptbl (plst:to-print-table true)
  # write to the interpreter output stream
  ptbl:format (interp:get-output-stream)
}

# write a property list to the output stream
# @param pstl the property list style
# @param plst the property list to write
const afnix:std:acl:write-style-plist (plst) {
  # convert the plist to a print table
  const ptbl (plst:to-print-table true (afnix:std:acl:get-default-style))
  # write to the interpreter output stream
  ptbl:format (interp:get-output-stream)
}

# write data to the output stream
# @param data the data to write
const afnix:std:acl:write-output-data (data) {
  # check for a string vector
  if (strvec-p data) {
    const os (interp:get-output-stream)
    for (s) (data) (os:writeln s)
  }
}

# dump a property list to the error stream for debug
# @param plst the property list to dump
const afnix:std:acl:write-error-plist (plst) {
  # convert the plist to a print table
  const ptbl (plst:to-print-table true)
  # write to the interpreter error stream
  ptbl:format (interp:get-error-stream)
}

# format a message to the output stream
# @param mesg the message to format
const afnix:std:acl:format-xio-message (mesg) {
  # make sure we have a message
  assert true (message-p mesg)
  # check for an info message
  switch (mesg:get-type) (
    (Message:INFO    (println (mesg:format)))
    (Message:WARNING (println (mesg:format)))
    (else            (errorln (mesg:format)))
  )
}
