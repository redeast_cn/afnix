// ---------------------------------------------------------------------------
// - Hyperp.hpp                                                              -
// - afnix engine - hyper interpreter class definition                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_HYPERP_HPP
#define  AFNIX_HYPERP_HPP

#ifndef  AFNIX_GRID_HPP
#include "Grid.hpp"
#endif

#ifndef  AFNIX_MODULE_HPP
#include "Module.hpp"
#endif

namespace afnix {

  /// The Hyperp class is a hyper threaded interpreter that operates with
  /// a grid. When a module is loaded, it is executed on a grid of threads
  /// that binds an interpreter.
  /// @author amaury darsch

  class Hyperp : public Object {
  protected:
    /// the hyper grid
    Grid d_grid;
    /// the hyper block
    Vector* p_hblk;
    /// the arguments vector
    Vector* p_argv;
    
  public:
    /// create a default hyper interpreter
    Hyperp (void);

    /// destroy this hyper interpreter
    ~Hyperp (void);
    
    /// @return the class name
    String repr (void) const override;

    /// @return the hyper interpreter grid
    virtual Grid getgrid (void) const;

    /// @return the hyper interpreter arguments
    virtual Vector* getargv (void) const;
    
    /// load a module in the hyper interpreter
    /// @param mp the module object to load
    virtual bool load (Module& mp);
    
  private:
    // make the copy constructor private
    Hyperp (const Hyperp&) =delete;
    // make the assignment operator private
    Hyperp& operator = (const Hyperp&);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
