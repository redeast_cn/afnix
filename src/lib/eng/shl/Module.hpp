// ---------------------------------------------------------------------------
// - Module.hpp                                                              -
// - afnix engine - module class definitions                                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_MODULE_HPP
#define  AFNIX_MODULE_HPP

#ifndef  AFNIX_FORMER_HPP
#include "Former.hpp"
#endif

#ifndef  AFNIX_NAMEABLE_HPP
#include "Nameable.hpp"
#endif

#ifndef  AFNIX_OUTPUTSTREAM_HPP
#include "OutputStream.hpp"
#endif

namespace afnix {

  /// The Module class is an in-core memory representation of an afnix file.
  /// The representation is either a text form or a serialized form. By 
  /// default the module constructor takes a name which is the input file
  /// name. The "parse" method returns the next form available within this 
  /// module. 
  /// @author amaury darsch

  class Module : public Nameable {
  public:
    /// supported module format
    enum t_mfmt
      {
	MFMT_NONE,
	MFMT_TEXT,
	MFMT_CBIN
      };

  private:
    /// the module format
    t_mfmt d_mfmt;
    /// the module name
    String  d_name;
    /// the form reader
    Former* p_frmr;

  public:
    /// create an empty module
    Module (void);

    /// create a module by name
    /// @param name the module name
    Module (const String& name);

    /// create a module by buffer
    /// @param ibuf the input buffer
    Module (const Buffer& ibuf);

    /// create a module by stream and name
    /// @param is   the input stream to use
    /// @param name the optional stream name
    Module (InputStream* is, const String& name);

    /// create a module by buffer and name
    /// @param ibuf the input buffer
    /// @param name the buffer name
    Module (const Buffer& ibuf, const String& name);

    /// destroy this module
    ~Module (void);

    /// @return the class name
    String repr (void) const override;

    /// bind a module by input stream
    /// @param is the input stream to bind
    virtual bool bind (InputStream* is);
    
    /// bind a module by input stream and name
    /// @param is the input stream to bind
    /// @param name the input name
    virtual bool bind (InputStream* is, const String& name);
    
    /// @return the next form
    virtual Form* parse (void);

    /// @return true for a text module
    virtual bool istext (void) const;

    /// @return the module format
    virtual t_mfmt getmfmt (void) const;

    /// @return the module name
    virtual String getname (void) const;

    /// @return the reader line number
    virtual long getlnum (void) const;

    /// write the module content
    virtual bool write (OutputStream& os);

    /// @return a module buffer content
    virtual Buffer tobuffer (void);
    
  private:
    // make the copy constructor private
    Module (const Module&) =delete;
    // make the assignment operator private
    Module& operator = (const Module&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
