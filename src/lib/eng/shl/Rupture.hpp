// ---------------------------------------------------------------------------
// - Rupture.hpp                                                             -
// - afnix engine - rupture exception class definition                       -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_RUPTURE_HPP
#define  AFNIX_RUPTURE_HPP

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {

  /// The Rupture class is a base class designed for specific flow control
  /// rupture, including return, break and continue. In principle, the class
  /// holds an object which is transported by an exception and treated then
  /// by the executing control environment.
  /// @author amaury darsch

  class Rupture : public Object {
  protected:
    /// the rupture object
    Object* p_robj;

  public:
    /// create a nil object
    Rupture (void);

    /// copy construct this rupture object
    /// @param that the rupture to copy
    Rupture (const Rupture& that);

    /// destroy this rupture object
    ~Rupture (void);

    /// assign a rupture object to this one
    /// @param that the object to assign
    Rupture& operator = (const Rupture& that);
    
    /// @return the returned object
    virtual Object* getrobj (void) const;
  };
  
  /// The Return class is a rupture object that is used to return from a
  /// an execution environment, typically a closure.
  /// @author amaury darsch

  class Return : public Rupture {
  public:
    /// create a nil object
    Return (void) =default;

    /// create a new return by object
    /// @param robj the object to return
    Return (Object* robj);

    /// copy constructor this object
    /// @param that the object to copy
    Return (const Return& that);

    /// @return the class name
    String repr (void) const override;

  private:
    // make the assignment operator private
    Return& operator = (const Return&) =delete;
  };

  /// The Break class is a rupture object that is used to break from a
  /// a loop or any other execution environment. In some cases, a break object
  /// behaves like a return object.
  /// @author amaury darsch

  class Break : public Rupture {
  public:
    /// create a nil object
    Break (void) =default;

    /// create a new break by object
    /// @param robj the object to return
    Break (Object* robj);

    /// copy construct this object
    /// @param that the object to copy
    Break (const Break& that);

    /// @return the class name
    String repr (void) const override;

  private:
    // make the assignment operator private
    Break& operator = (const Break&) =delete;
  };
  
  /// The Continue class is a rupture object that is used to break from a
  /// a loop or any other execution environment and restart it. In some cases,
  /// a continue object behaves like a return object.
  /// @author amaury darsch

  class Continue : public Rupture {
  public:
    /// create a nil object
    Continue (void) =default;

    /// create a new continue by object
    /// @param robj the object to return
    Continue (Object* robj);

    /// copy construct this object
    /// @param that the object to copy
    Continue (const Continue& that);

    /// @return the class name
    String repr (void) const override;

  private:
    // make the assignment operator private
    Continue& operator = (const Continue&) =delete;
  };
}

#endif
