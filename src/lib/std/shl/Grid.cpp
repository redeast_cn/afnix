// ---------------------------------------------------------------------------
// - Grid.cpp                                                                -
// - standard object library - grid class implementation                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Grid.hpp"
#include "Vector.hpp"
#include "Integer.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "array.tcc"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default grid
  
  Grid::Grid (void) {
    p_dims = new t_array<long>;
    p_dims->push (1L);
  }

  // create a null grid by size

  Grid::Grid (const long size) {
    if (size <= 0L) {
      throw Exception ("grid-error", "invalid grid size");
    }
    p_dims = new t_array<long>;
    for (long k = 0; k < size; k++) p_dims->push (0L);
  }
  
  // copy construct a grid
  
  Grid::Grid (const Grid& that) {
    that.rdlock ();
    try {
      p_dims = new t_array<long>(*that.p_dims);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // destroy this grid

  Grid::~Grid (void) {
    delete p_dims;
  }
  
  // assign a value object to this one
  
  Grid& Grid::operator = (const Grid& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      delete p_dims; p_dims = new t_array<long>(*that.p_dims);
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // compare two grids

  bool Grid::operator == (const Grid& that) const {
    rdlock ();
    that.rdlock ();
    try {
      // check grid length first
      if (p_dims->length () != that.p_dims->length ()) {
	unlock ();
	that.unlock ();
	return false;
      }	
      // loop in length
      long glen = p_dims->length ();
      for (long k = 0L; k < glen; k++) {
	if (p_dims->get(k) != that.p_dims->get(k)) {
	  unlock ();
	  return false;
	}
      }
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    } 
  }

  // compare two grids

  bool Grid::operator < (const Grid& that) const {
    rdlock ();
    that.rdlock ();
    try {
      // check grid length first
      long glen = p_dims->length ();
      if (glen != that.p_dims->length ()) {
	throw Exception ("grid-error", "incompatible grid length to compare");
      }	
      // loop in length
      for (long k = 0L; k < glen; k++) {
	if (p_dims->get(k) >= that.p_dims->get(k)) {
	  unlock ();
	  return false;
	}
      }
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    } 
  }

  // compare two grids

  bool Grid::operator <= (const Grid& that) const {
    rdlock ();
    that.rdlock ();
    try {
      // check grid length first
      long glen = p_dims->length ();
      if (glen != that.p_dims->length ()) {
	throw Exception ("grid-error", "incompatible grid length to compare");
      }
      // loop in length
      for (long k = 0L; k < glen; k++) {
	if (p_dims->get(k) > that.p_dims->get(k)) {
	  unlock ();
	  return false;
	}
      }
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    } 
  }
  
  // return the object class name
  
  String Grid::repr (void) const {
    return "Grid";
  }

  // get a clone of this object

  Object* Grid::clone (void) const {
    return new Grid (*this);
  }

  // get the grid length

  long Grid::length (void) const {
    rdlock ();
    try {
      long result = p_dims->length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the grid dimension

  long Grid::getdims (void) const {
    rdlock ();
    try {
      long result = 0L; long glen = p_dims->length();
      for (long k = 0L; k < glen; k++) result += p_dims->get(k);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get a grid dimension by index

  long Grid::getdims (const long didx) const {
    rdlock ();
    try {
      if ((didx < 0) || (didx >= p_dims->length ())) {
	throw Exception ("index-error", "invalid grid dimension index");
      }
      long result = p_dims->get (didx);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // create a new grid at the beginning

  Grid Grid::begin (void) const {
    rdlock ();
    try {
      Grid result (p_dims->length());
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compute the next grid position

  Grid Grid::next (const Grid& grid) const {
    rdlock ();
    try {
      // check grid length first
      long glen = p_dims->length ();
      if (glen != grid.p_dims->length ()) {
	throw Exception ("grid-error", "incompatible grid length in next");
      }
      // check if we are at the end
      if (grid == *this) {
	unlock ();
	return grid;
      }
      // create a result grid
      Grid result (grid);
      // loop in grid length
      for (long k = 0L; k < glen; k++) {
	long gval = result.p_dims->get(k) + 1L;
	if (gval < p_dims->get(k)) {
	  result.p_dims->set (k, gval);
	  unlock ();
	  return result;
	}
	result.p_dims->set(k, 0L);
      }
      // here we are at the end
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_LENGTH = zone.intern ("length");
  static const long QUARK_GETDIM = zone.intern ("dimension");
  
  // create a new object in a generic way
  
  Object* Grid::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Grid;
    // invalid arguments
    throw Exception ("argument-error",
                     "too many argument with value constructor");
  }
  
  // return true if the given quark is defined
  
  bool Grid::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
        unlock ();
        return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark
  
  Object* Grid::apply (Evaluable* zobj, Nameset* nset, const long quark,
		       Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_LENGTH) return new Integer (length  ());
      if (quark == QUARK_GETDIM) return new Integer (getdims ());
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_GETDIM) {
	long didx = argv->getlong (0);
	return new Integer (getdims (didx));
      }
    }
    // call the object method
    return Object::apply (zobj, nset, quark, argv);
  }
}
