// ---------------------------------------------------------------------------
// - Transition.hpp                                                          -
// - afnix:gfx module - state transition class definition                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TRANSITION_HPP
#define  AFNIX_TRANSITION_HPP

#ifndef  AFNIX_STATE_HPP
#include "State.hpp"
#endif

#ifndef  AFNIX_GLOBAL_HPP
#include "Global.hpp"
#endif

namespace afnix {

  /// The Transition class is a state transition computation class. Given a
  /// state, the transition object compute the next state index for the
  /// automaton. Similar to the state object, a transition can store a client
  /// object.
  /// @author amaury darsch

  class Transition : public virtual Serial {
  protected:
    /// the client object
    Object* p_cobj;
    
  public:
    /// create a default transition
    Transition (void);

    /// create a transition with a client object
    /// @param cobj the client object
    Transition (Object* cobj);

    /// destroy this 
    ~Transition (void);

    /// @return the class name
    String repr (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// serialize this uuid
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this uuid
    /// @param is the input stream
    void rdstream (InputStream& os) override;
    
    /// @return the next state index
    virtual long nxsidx (Global* glob, State* stte) const;
    
    /// set the transition client object
    /// @param clo the client object
    virtual void setcobj (Object* cobj);

    /// @return the transition client object
    virtual Object* getcobj (void) const;

  private:
    // make the copy constructor private
    Transition (const Transition&) =delete;
    // make the assignment operator private
    Transition& operator = (const Transition&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
