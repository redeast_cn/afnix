// ---------------------------------------------------------------------------
// - Automaton.hpp                                                           -
// - standard object module - automaton class definition                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_AUTOMATON_HPP
#define  AFNIX_AUTOMATON_HPP

#ifndef  AFNIX_BITS_HPP
#include "Bits.hpp"
#endif

#ifndef  AFNIX_ITERABLE_HPP
#include "Iterable.hpp"
#endif

#ifndef  AFNIX_TRANSITION_HPP
#include "Transition.hpp"
#endif

namespace afnix {

  /// The Automaton class is a simple automaton designed to store a pair of
  /// state and transition (stap). Unlike a traditional automaton, the
  /// transition object is an interface which computes the next state index.
  /// If there is no transition, the next state is computed from the current
  /// state index.
  /// @author amaury darsch

  class Automaton : public virtual Serial, public Iterable {
  protected:
    /// the global object
    Global* p_glob;
    /// the state/transition array
    t_array<struct s_stap>* p_stap;
    
  public:
    /// create an empty automaton
    Automaton (void);

    /// create an automaton by global
    /// @param the global object
    Automaton (Global* glob);

    /// copy construct this automaton
    /// @param that the object to copy
    Automaton (const Automaton& that);
    
    /// destroy this automaton
    ~Automaton (void);

    /// assign an automaton to this one
    /// @param that the object to assign
    Automaton& operator = (const Automaton&);

    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// serialize this uuid
    /// @param os the output stream
    void wrstream (OutputStream& os) const override ;

    /// deserialize this uuid
    /// @param is the input stream
    void rdstream (InputStream& os) override;
    
    /// reset the automaton
    virtual void reset (void);

    /// bind a global to the automaton
    /// @parfam glob the global to bind
    virtual bool bind (Global* glob);
    
    /// @return the automaton length
    virtual long length (void) const;

    /// add a state to this automaton.
    /// @param stte the state to add
    virtual void add (State* stte);

    /// add a state to this automaton.
    /// @param stte the state to add
    /// @param trnt the transition to add
    virtual void add (State* stte, Transition* trnt);

    /// @return a state by stap index
    virtual State* getstte (const long sidx) const;

    /// @return a transition by stap index
    virtual Transition* gettrnt (const long sidx) const;
    
  public:
    /// @return a new iterator for this automaton
    Iterator* makeit (void) override;

    /// create an iterator at the begining
    class Automatic begin (void);

    /// create an iterator at the end
    class Automatic end (void);

  private:
    /// make the iterator a friend
    friend class Automatic;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
  
  /// The Automatic class is the iterator for the vector class. Such 
  /// iterator is constructed with the "makeit" vector method. The iterator
  /// is reset to the beginning of the vector.
  /// @author amaury darsch

  class Automatic : public Iterator {
  private:
    /// the automaton to iterate
    Automaton* p_aobj;
    /// the iterator stap index
    long d_sidx;

  public:
    /// create a new iterator from an automaton
    /// @param aobj the automaton to iterate
    Automatic (Automaton* aobj);

    /// copy construct this iterator
    /// @param that the object to copy
    Automatic (const Automatic& that);
    
    /// destroy this vector iterator
    ~Automatic (void);

    /// assign an iterator to this one
    /// @param that the object to assign
    Automatic& operator = (const Automatic& that);

    /// compare two iterators
    /// @param it the iteraror to compare
    bool operator == (const Automatic& it) const;

    /// compare two iterators
    /// @param it the iteraror to compare
    bool operator != (const Automatic& it) const;

    /// move the iterator to the next position
    Automatic& operator ++ (void);
    
    /// move the iterator to the previous position
    Automatic& operator -- (void);
    
    /// @return the iterator object
    Object* operator * (void) const;
    
    /// @return the class name
    String repr (void) const override;

    /// reset the iterator to the begining
    void begin (void) override;

    /// reset the iterator to the end
    void end (void) override;

    /// move the vector iterator to the next position
    void next (void) override;

    /// move the vector iterator to the previous position
    void prev (void) override;

    /// @return the object at the current position
    Object* getobj (void) const override;

    /// @return true if the iterator is at the end
    bool isend (void) const override;

    /// @return the iterator client object
    virtual Object* getcobj (void) const;
  };
}

#endif
