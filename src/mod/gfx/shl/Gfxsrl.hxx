// ---------------------------------------------------------------------------
// - Gfxsrl.hxx                                                              -
// - afnix:gfx module - serial decoding definition                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_GFXSRL_HXX
#define  AFNIX_GFXSRL_HXX

#include "Gfxsid.hxx"
#include "Utility.hpp"
#include "Automaton.hpp"
#include "Exception.hpp"

namespace afnix {
  
  // the gfx dispatch function
  static Serial* srl_deod_gfx (const t_word sid) {
    switch (sid) {
    case SRL_STTE_SID:
      return new State;
      break;
    case SRL_TRNT_SID:
      return new Transition;
      break;
    case SRL_ATMT_SID:
      return new Automaton;
      break;
    default:
      break;
    }
    throw Exception ("serial-error", "invalid gfx sid code",
		     Utility::tohexa (sid, true, true));
  }
  
  // bind the dispatch function
  static t_word SRL_DEOD_DID = Serial::addsd (SRL_DEOD_GFX, srl_deod_gfx);
}

#endif
