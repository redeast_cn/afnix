# ---------------------------------------------------------------------------
# - SEC0016.als                                                             -
# - afnix:sec module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   pkcs test unit
# @author amaury darsch

# get the module
interp:library "afnix-sec"

# global key type
const KRSA afnix:sec:Key:KRSA

# the key parameters
const m 0xC8A20691_82394A2A_B7C3F419_0C15589C_56A2D4BC_42DCA675◀
         ▶B34CC950_E2466304_8441E8AA_593B2BC5_9E198B8C_257E8821◀
         ▶20C62336_E5CC7450_12C7FFB0_63EEBE53_F3C6504C_BA6CFE51◀
         ▶BAA3B6D1_074B2F39_8171F4B1_982F4D65_CAF882EA_4D56F32A◀
         ▶B57D0C44_E6AD4E9C_F57A4339_EB696240_6E350C1B_15397183◀
         ▶FBF1F035_3C9FC991R

const e 0x00010001R
         
const s 0x5DFCB111_072D2956_5BA1DB3E_C48F5764_5D9D8804_ED598A4D◀
         ▶470268A8_9067A2C9_21DFF24B_A2E37A3C_E8345550_00DC868E◀
         ▶E6588B74_93303528_B1B3A94F_0B71730C_F1E86FCA_5AEEDC3A◀
         ▶FA16F65C_0189D810_DDCD8104_9EBBD039_1868C50E_DEC958B3◀
         ▶A2AAEFF6_A575897E_2F20A3AB_5455C1BF_A55010AC_51A7799B◀
         ▶1FF84836_44A3D425R

# the message and signature
const M1 "E8312742AE23C456EF28A23142C4490895832765DADCE02AFE5BE5D31B0048FBEEE"
const M2 "2CF218B1747AD4FD81A2E17E124E6AF17C3888E6D2D40C00807F423A233CAD62CE9"
const M3 "EAEFB709856C94AF166DBA08E7A06965D7FC0D8E5CB26559C460E47BC088589D224"
const M4 "2C9B3E62DA4896FAB199E144EC136DB8D84AB84BCBA04CA3B90C8E5"
const M  (+ M1 (+ M2 (+ M3 M4)))

const S 0x28928E19_EB86F9C0_0070A59E_DF6BF843_3A45DF49_5CD1C736◀
         ▶13C21298_40F48C4A_2C24F11D_F79BC5C0_782BCEDD_E97DBBB2◀
         ▶ACC6E512_D19F0850_27CD5750_38453D04_905413E9_47E6E1DD◀
         ▶DBEB3535_CDB3D897_1FE02005_06941056_F2124350_3C83EADD◀
         ▶E053ED86_6C0E0250_BEDDD927_A08212AA_8AC0EFD6_1631EF89◀
         ▶D8D049EF_B36BB35FR

# key verification
assert 1024 (m:get-msb)

# create the rsa key
const  key (afnix:sec:Key KRSA (Vector m e s))

# create the pkcs object
trans  pkcs (afnix:sec:Pkcs key)
assert true (afnix:sec:pkcs-p pkcs)

# derive from an octet string
const  sgn  (pkcs:derive M)
assert true (afnix:sec:signature-p sgn)
# check the signature
assert true (pkcs:check sgn M)

# get the signature component
assert S (sgn:get-relatif-component afnix:sec:Signature:RSA-S-COMPONENT)
