# ---------------------------------------------------------------------------
# - MTH0703.als                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   real trace test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"

# create a simple trace
const rt (afnix:mth:Rtrace 3)
# check predicate
assert true (afnix:mth:rti-p rt)
assert true (afnix:mth:r-trace-p rt)
# check representation
assert "Rtrace" (rt:repr)
# check trace length and predicate
assert 3 (rt:get-size)
assert true (rt:nil-p)

# check accessors
assert 0.0 (rt:get 0)
rt:set 0 0.0
assert 0.0 (rt:get 0)

assert 0.0 (rt:get 1)
rt:set 1 3.0
assert 3.0 (rt:get 1)

assert 0.0 (rt:get 2)
rt:set 2 4.0
assert 4.0 (rt:get 2)

# check matrix constructor
trans rm (afnix:mth:Rblock rt)
assert 0.0 (rm:get 0 0)
assert 3.0 (rm:get 1 1)
assert 4.0 (rm:get 2 2)
# check matrix constructor
trans rm (afnix:mth:Rmatrix rt)
assert 0.0 (rm:get 0 0)
assert 3.0 (rm:get 1 1)
assert 4.0 (rm:get 2 2)

# create another trace
const nv (afnix:mth:Rtrace 3)
nv:set 0 1.0
nv:set 1 1.0
nv:set 2 1.0

# check the operators
trans av (+ rt nv)
assert 1.0 (av:get 0)
assert 4.0 (av:get 1)
assert 5.0 (av:get 2)

trans sv (- av nv)
assert true (sv:?= rt)

trans av (* rt 2.0)
trans av (/ av 2.0)

assert 0.0 (rt:get 0)
assert 3.0 (rt:get 1)
assert 4.0 (rt:get 2)

# create a permutation
const p (afnix:mth:Permute 3)
p:set 0 2
p:set 1 0
p:set 2 1

# permutate the trace
const pv (rt:permutate p)
assert 4.0 (pv:get 0)
assert 0.0 (pv:get 1)
assert 3.0 (pv:get 2)

# do a reverse permutation
const vp (pv:reverse p)
assert true (rt:?= vp)
