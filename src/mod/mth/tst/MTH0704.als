# ---------------------------------------------------------------------------
# - MTH0704.als                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   complex trace test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"

# create a simple trace
const ct (afnix:mth:Ctrace 3)
# check predicate
assert true (afnix:mth:cti-p ct)
assert true (afnix:mth:c-trace-p ct)
# check representation
assert "Ctrace" (ct:repr)
# check trace length and predicate
assert 3 (ct:get-size)
assert true (ct:nil-p)

# check accessors
assert 0.0+0.0i (ct:get 0)
ct:set 0 0.0
assert 0.0+0.0i (ct:get 0)

assert 0.0+0.0i (ct:get 1)
ct:set 1 3.0+0.0i
assert 3.0+0.0i (ct:get 1)

assert 0.0+0.0i (ct:get 2)
ct:set 2 0.0+4.0i
assert 0.0+4.0i (ct:get 2)

# create another trace
const nv (afnix:mth:Ctrace 3)
nv:set 0 1.0
nv:set 1 1.0
nv:set 2 1.0

# check the operators
trans av (+ ct nv)
assert 1.0+0.0i (av:get 0)
assert 4.0+0.0i (av:get 1)
assert 1.0+4.0i (av:get 2)

trans sv (- av nv)
assert true (sv:?= ct)

trans av (* ct 2.0)
trans av (/ av 2.0)

assert 0.0+0.0i (ct:get 0)
assert 3.0+0.0i (ct:get 1)
assert 0.0+4.0i (ct:get 2)

# create a permutation
const p (afnix:mth:Permute 3)
p:set 0 2
p:set 1 0
p:set 2 1

# permutate the trace
const pv (ct:permutate p)
assert 0.0+4.0i (pv:get 0)
assert 0.0+0.0i (pv:get 1)
assert 3.0+0.0i (pv:get 2)

# do a reverse permutation
const vp (pv:reverse p)
assert true (ct:?= vp)
