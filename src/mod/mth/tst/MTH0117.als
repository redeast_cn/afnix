# ---------------------------------------------------------------------------
# - MTH0117.als                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   complex vector test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"

# create a simple vector
const cv (afnix:mth:Cvector 3)
# check predicate
assert true (afnix:mth:cvi-p cv)
assert true (afnix:mth:c-vector-p cv)
# check representation
assert "Cvector" (cv:repr)
# check vector length and predicate
assert 3 (cv:get-size)
assert true (cv:nil-p)

# check accessors
assert 0.0+0.0i (cv:get 0)
cv:set 0 0.0
assert 0.0+0.0i (cv:get 0)

assert 0.0+0.0i (cv:get 1)
cv:set 1 3.0+0.0i
assert 3.0+0.0i (cv:get 1)

assert 0.0+0.0i (cv:get 2)
cv:set 2 0.0+4.0i
assert 0.0+4.0i (cv:get 2)

# check dedicated methods
assert 5.0 (cv:norm)

# test the vector operators
cv:*= 2.0
assert 10.0 (cv:norm)

cv:+= 1.0
cv:-= 1.0
cv:/= 2.0
assert 5.0 (cv:norm)

# create another vector
const nv (afnix:mth:Cvector 3)
nv:set 0 1.0
nv:set 1 1.0
nv:set 2 1.0

# check the dot product
assert 3.0+4.0i (cv:dot nv)

# check the operators
trans av (+ cv nv)
assert 1.0+0.0i (av:get 0)
assert 4.0+0.0i (av:get 1)
assert 1.0+4.0i (av:get 2)

trans sv (- av nv)
assert true (sv:?= cv)

trans av (* cv 2.0)
assert 10.0 (av:norm)
trans av (/ av 2.0)
assert 5.0 (av:norm)

assert 0.0+0.0i (cv:get 0)
assert 3.0+0.0i (cv:get 1)
assert 0.0+4.0i (cv:get 2)

# create a permutation
const p (afnix:mth:Permute 3)
p:set 0 2
p:set 1 0
p:set 2 1

# permutate the vector
const pv (cv:permutate p)
assert 0.0+4.0i (pv:get 0)
assert 0.0+0.0i (pv:get 1)
assert 3.0+0.0i (pv:get 2)

# do a reverse permutation
const vp (pv:reverse p)
assert true (cv:?= vp)
