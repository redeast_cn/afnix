// ---------------------------------------------------------------------------
// - Nti.hpp                                                                 -
// - afnix:mth module - numeral trace interface definitions                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_NTI_HPP
#define  AFNIX_NTI_HPP

#ifndef  AFNIX_ATI_HPP
#include "Ati.hpp"
#endif

#ifndef  AFNIX_NUMERAL_HPP
#include "Numeral.hpp"
#endif

namespace afnix {

  /// This Nti class is an abstract class that models the behatior of a
  /// numeral based trace. The class extends the abstract trace interface
  /// with specific numeral methods.
  /// @author amaury darsch

  class Nti : public Ati {
  protected:
    /// the numeral type
    Numeral::t_numt d_numt;

  public:
    /// create a null trace
    Nti (void);

    /// create a trace by size
    /// @param size the trace size
    Nti (const t_long size);

    /// create a trace by size and type
    /// @param size the trace size
    /// @param numt the numeral type
    Nti (const t_long size, const Numeral::t_numt numt);

    /// copy construct this trace
    /// @param that the trace to copy
    Nti (const Nti& that);

    /// copy move this trace
    /// @param that the trace to move
    Nti (Nti&& that) noexcept;

    /// assign a trace into this one
    /// @param that the trace to assign
    Nti& operator = (const Nti& that);

    /// move a trace into this one
    /// @param that the trace to move
    Nti& operator = (Nti&& that) noexcept;

    /// serialize this object
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this object
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// reset this trace
    void reset (void) override;

    /// clear this trace
    void clear (void) override;

    /// @return true if the trace is nil
    bool isnil (void) const override;

    /// @return a vector of literals
    Vector tovector (void) const override;

    /// @return the trace format
    String tofrmt (void) const override;

    /// set an identity trace
    virtual void eye (void);
    
    /// set the trace by numeral value
    /// @param val the value to set
    virtual void set (const Numeral& val);

    /// set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    virtual void set (const t_long pos, const Numeral& val);

    /// get a trace value by position
    /// @param pos the trace position
    virtual Numeral get (const t_long pos) const;

    /// get a trace integer value by position
    /// @param pos the trace position
    virtual t_long getlong (const t_long pos) const;

    /// get a trace real value by position
    /// @param pos the trace position
    virtual t_real getreal (const t_long pos) const;

    /// convert a numeral trace by type
    /// @param numt the target type
    virtual Numeral::t_numt convert (const Numeral::t_numt numt) =0;

  public:
    /// no lock - set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    virtual void nlset (const t_long pos, const Numeral& val) =0;

    /// no lock - get a trace value by position
    /// @param pos the trace position
    virtual Numeral nlget (const t_long pos) const =0;

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
