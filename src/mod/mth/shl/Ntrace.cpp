// ---------------------------------------------------------------------------
// - Ntrace.cpp                                                              -
// - afnix:mth module - numeral trace implementation                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Ntrace.hpp"
#include "Algebra.hpp"
#include "Utility.hpp"
#include "Exception.hpp"
 
namespace afnix {
      
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default trace

  Ntrace::Ntrace (void) {
    p_btab = nullptr;
  }

  // create a trace by size

  Ntrace::Ntrace (const t_long size) : Nti (size) {
    p_btab = nullptr;
    preset ();
  }
  
  // create a trace by size and type

  Ntrace::Ntrace (const t_long size,
		  const Numeral::t_numt numt) : Nti (size, numt) {
    p_btab = nullptr;
    preset ();
  }
  
  // copy construct this trace

  Ntrace::Ntrace (const Ntrace& that) {
    that.rdlock ();
    try {
      // copy base class
      Nti::operator = (that);
      // copy locally
      p_btab = nullptr;
      t_long size = d_size * Numeral::tobsiz(d_numt);
      if (size > 0LL) {
	p_btab = new t_byte[size];
	Utility::tobcpy (p_btab, size, that.p_btab);
      }
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
	
  // destroy this trace

  Ntrace::~Ntrace (void) {
    delete [] p_btab;
  }

  // assign a trace to this one

  Ntrace& Ntrace::operator = (const Ntrace& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // clean locally
      delete [] p_btab; p_btab = nullptr;
      // assign base class
      Nti::operator = (that);
      // assign locally
      if (d_size > 0) {
	long size = d_size * Numeral::tobsiz(d_numt);
	p_btab = new t_byte[size];
	Utility::tobcpy (p_btab, size, that.p_btab);
      }
      // unlock and return
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // return the class name

  String Ntrace::repr (void) const {
    return "Ntrace";
  }

  // return a clone of this object

  Object* Ntrace::clone (void) const {
    return new Ntrace (*this);
  }
  
  // return the serial did

  t_word Ntrace::getdid (void) const {
    return SRL_DEOD_MTH;
  }

  // return the serial sid

  t_word Ntrace::getsid (void) const {
    return SRL_NTRC_SID;
  }

  // reset this trace

  void Ntrace::reset (void) {
    wrlock ();
    try {
      // reset base
      Nti::reset ();
      // reset locally
      d_size = 0L;
      delete [] p_btab; p_btab = nullptr;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this trace

  void Ntrace::clear (void) {
    wrlock ();
    try {
      t_long size = d_size * Numeral::tobsiz (d_numt);
      for (t_long k = 0L; k < size; k++) p_btab[k] = nilb;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // resize this trace

  void Ntrace::resize (const t_long size) {
    wrlock ();
    try {
      if (size < 0) {
	throw Exception ("rtrace-error", "invalid negatize size in resize");
      }
      if (size == 0)  {
	delete [] p_btab;
	d_size = 0L;
	p_btab = nullptr;
	unlock ();
	return;
      }
      // do nothing if equal
      if (size == d_size) {
	unlock ();
	return;
      }
      // create a new array by size
      t_long  bsiz = size * Numeral::tobsiz (d_numt);
      t_byte* btab = new t_byte[bsiz];
      // check for smaller size
      if (size < d_size) {
	Utility::tobcpy (btab, bsiz, p_btab);
      } else {
	t_long osiz = d_size * Numeral::tobsiz (d_numt);
	Utility::tobcpy (btab, osiz, p_btab);
	for (long k = osiz; k < bsiz; k++) btab[k] = nilb;
      }
      d_size = size;
      delete [] p_btab;
      p_btab = btab;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // preset this trace

  void Ntrace::preset (void) {
    wrlock ();
    try {
      delete [] p_btab;
      t_long size = d_size * Numeral::tobsiz (d_numt);
      p_btab = (size == 0LL) ? nullptr : new t_byte[size];
      clear ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the viewable size

  long Ntrace::tosize (void) const {
    rdlock ();
    try {
      long result = d_size * Numeral::tobsiz (d_numt);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the viewable data

  t_byte* Ntrace::tobyte (void) {
    wrlock ();
    try {
      t_byte* result = p_btab;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the viewable data

  const t_byte* Ntrace::tobyte (void) const {
    rdlock ();
    try {
      const t_byte* result = p_btab;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert a trace into another type

  Numeral::t_numt Ntrace::convert (const Numeral::t_numt numt) {
    wrlock ();
    try {
      // check for same type
      if (d_numt == numt) {
	unlock ();
	return numt;
      }
      // check for integer
      if (d_numt == Numeral::NUMT_SINT) {
	if (numt == Numeral::NUMT_SFLT) {
	  for (long k = 0L; k < d_size; k++) p_ftab[k] = (float) p_itab[k];
	}
      }
      // check for float
      if (d_numt == Numeral::NUMT_SFLT) {
	if (numt == Numeral::NUMT_SINT) {
	  for (long k = 0L; k < d_size; k++) p_itab[k] = (int) p_ftab[k];
	}
      }
      Numeral::t_numt result = d_numt;
      d_numt = numt;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - no lock section                                                        -
  // -------------------------------------------------------------------------

  // no lock - set a trace by position

  void Ntrace::nlset (const t_long pos, const Numeral& val) {
    switch (d_numt) {
    case Numeral::NUMT_SINT:
      if (p_itab != nullptr) p_itab[pos] = (int) val.tolong();
      break;
    case Numeral::NUMT_SFLT:
      if (p_ftab != nullptr) p_ftab[pos] = (float) val.toreal();
      break;
    }
  }

  // no lock - get a trace by position
  
  Numeral Ntrace::nlget (const t_long pos) const {
    if (p_btab == nullptr) return Numeral (d_numt);
    switch (d_numt) {
    case Numeral::NUMT_SINT:
      return Numeral((t_long) p_itab[pos]);
      break;
    case Numeral::NUMT_SFLT:
      return Numeral((t_real) p_ftab[pos]);
      break;
    }
    return Numeral(d_numt);
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way

  Object* Ntrace::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) return new Ntrace;
    // check for 1 argument
    if (argc == 1) {
      t_long size = argv->getlong (0);
      return new Ntrace (size);
    }
    // check for 2 arguments
    if (argc == 2) {
      t_long size = argv->getlong (0);
      Object* obj = argv->get (1);
      auto   item = dynamic_cast<Item*>(obj);
      if (item == nullptr) {
	throw Exception ("type-error", "invalid object with trace",
			 Object::repr (obj));
      }
      Numeral::t_numt numt = Numeral::tonumt(*item);
      return new Ntrace (size, numt);
    }
    // invalid arguments
    throw Exception ("argument-error", 
		     "invalid arguments with numeral trace object");
  }
}

