// ---------------------------------------------------------------------------
// - Hilbert.hpp                                                             -
// - afnix:mth module - hilber space algorithm definitions                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_HILBERT_HPP
#define  AFNIX_HILBERT_HPP

#ifndef  AFNIX_CMI_HPP
#include "Cmi.hpp"
#endif

namespace afnix {

  /// The Hilbert class is a class that implements various algorithms used
  /// in the context of hilbert space. It is here that te tensorial product
  /// is implemented.
  /// @author amaury darsch

  class Hilbert {
  public:
    /// @return true if a matrix is unitary
    static bool unitary (const Cmi& m);

    /// compute the tensor product of two vectors
    /// @param m the result matrix
    /// @param x the ket vector
    /// @param y the bra vector
    static void product (Cmi& m, const Cvi& x, const Cvi& y);

    /// compute the tensor product of two matrices
    /// @param mr the result matrix
    /// @param mx the matrix argument
    /// @param my the matrix argument
    static void product (Cmi& mr, const Cmi& mx, const Cmi& my);
  };
}

#endif
