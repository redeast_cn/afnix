// ---------------------------------------------------------------------------
// - Ntrace.hpp                                                              -
// - afnix:mth module - numeral trace definitions                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_NTRACE_HPP
#define  AFNIX_NTRACE_HPP

#ifndef  AFNIX_NTI_HPP
#include "Nti.hpp"
#endif
 
#ifndef  AFNIX_VIEWABLE_HPP
#include "Viewable.hpp"
#endif
 
namespace afnix {

  /// This Ntrace class is the default implementation of the numeral trace
  /// interface. Internally, the trace is represented as an array of integer
  /// or floats.
  /// @author amaury darsch

  class Ntrace : public Nti, public Viewable {
  protected:
    /// the trace elements
    union {
      /// the integer trace
      int*    p_itab;
      /// the float trace
      float*  p_ftab;
      /// the byte trace
      t_byte* p_btab;
    };
    
  public:
    /// create a null trace
    Ntrace (void);

    /// create a trace by size
    /// @param size the trace size
    Ntrace (const t_long size);

    /// create a trace by size and type
    /// @param size the trace size
    /// @param numt the numeral type
    Ntrace (const t_long size, const Numeral::t_numt numt);

    /// copy construct this trace
    /// @param that the object to copy
    Ntrace (const Ntrace& that);

    /// destroy this trace
    ~Ntrace (void);

    /// assign a trace to this one
    /// @param that the object to assign
    Ntrace& operator = (const Ntrace& that);
    
    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// reset this trace
    void reset (void) override;

    /// clear this trace
    void clear (void) override;

    /// resize this trace
    /// @param size the new trace size
    void resize (const t_long size) override;

    /// preset this trace
    void preset (void) override;

    /// @return the viewable size
    long tosize (void) const override;

    /// @return the viewable data
    t_byte* tobyte (void) override;
    
    /// @return the viewable data
    const t_byte* tobyte (void) const override;
    
    /// convert a numeral trace by type
    /// @param numt the target type
    Numeral::t_numt convert (const Numeral::t_numt numt) override;
    
  public:
    /// no lock - set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    void nlset (const t_long pos, const Numeral& val) override;

    /// no lock - get a trace value by position
    /// @param pos the trace position
    Numeral nlget (const t_long pos) const override;

  public:
    /// create a new object in a generic way
    /// @param argv the argument trace
    static Object* mknew (Vector* argv);
  };
}

#endif
