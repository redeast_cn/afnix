// ---------------------------------------------------------------------------
// - Ctrace.hpp                                                              -
// - afnix:mth module - complex trace definitions                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CTRACE_HPP
#define  AFNIX_CTRACE_HPP

#ifndef  AFNIX_CTI_HPP
#include "Cti.hpp"
#endif
 
#ifndef  AFNIX_VIEWABLE_HPP
#include "Viewable.hpp"
#endif
 
namespace afnix {

  /// This Ctrace class is the default implementation of the real trace
  /// interface. Internally, the trace is represented as an array of complex.
  /// @author amaury darsch

  class Ctrace : public Cti, public Viewable {
  public:
    /// add a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar argument
    friend Ctrace operator + (const Ctrace& x, const Complex& s);

    /// add a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    friend Ctrace operator + (const Ctrace& x, const Ctrace& y);

    /// substract a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar argument
    friend Ctrace operator - (const Ctrace& x, const Complex& s);

    /// substract a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    friend Ctrace operator - (const Ctrace& x, const Ctrace& y);

    /// multiply a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar argument
    friend Ctrace operator * (const Ctrace& x, const Complex& s);

    /// divide a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar argument
    friend Ctrace operator / (const Ctrace& x, const Complex& s);

  protected:
    /// the trace elements
    t_real* p_vtab;

  public:
    /// create a null trace
    Ctrace (void);

    /// create a trace by size
    /// @param size the trace size
    Ctrace (const t_long size);

    /// copy construct this trace
    /// @param that the object to copy
    Ctrace (const Ctrace& that);

    /// destroy this trace
    ~Ctrace (void);

    /// assign a trace to this one
    /// @param that the object to assign
    Ctrace& operator = (const Ctrace& that);

    /// compare two traces
    /// @param  x the trace argument
    /// @return true if they are equals
    bool operator == (const Cti& x) const override;

    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// resize this trace
    /// @param size the new trace size
    void resize (const t_long size) override;

    /// reset this trace
    void reset (void) override;

    /// clear this trace
    void clear (void) override;

    /// preset this trace
    void preset (void) override;

    /// copy a trace into this one
    /// @param x the trace to copy
    Cti& cpy (const Cti& x) override;

    /// add a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar factor
    Cti& add (const Cti& x, const Complex& s) override;

    /// add a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    Cti& add (const Cti& x, const Cti& y) override;

    /// add a trace with another scaled one
    /// @param x the trace argument
    /// @param y the trace argument
    /// @param s the scalar factor
    Cti& add (const Cti& x, const Cti& y, const Complex& s) override;

    /// substract a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar factor
    Cti& sub (const Cti& x, const Complex& s) override;

    /// substract a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    Cti& sub (const Cti& x, const Cti& y) override;

    /// multiply a trace with a scaled trace
    /// @param x the trace to multiply
    /// @param s the scaling factor
    Cti& mul (const Cti& x, const Complex& s) override;

    /// multiply a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    Cti& mul (const Cti& x, const Cti& y) override;

    /// divide a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    Cti& div (const Cti& x, const Cti& y) override;

    /// add equal with a trace
    /// @param x the trace to add
    Cti& aeq (const Cti& x) override;

    /// add equal with a scaled trace
    /// @param x the trace to add
    /// @param s the scaling factor
    Cti& aeq (const Cti& x, const Complex& s) override;

    /// resize equal with a scaled trace
    /// @param x the trace to add
    /// @param s the scaling factor
    Cti& req (const Cti& x, const Complex& s) override;

    /// permutate this trace
    /// @param p the permutation object
    Cti* permutate (const Cpi& p) const override;
    
    /// reverse this trace permutation
    /// @param p the permutation object
    Cti* reverse (const Cpi& p) const override;

    /// @return the viewable size
    long tosize (void) const override;

    /// @return the viewable data
    t_byte* tobyte (void) override;
    
    /// @return the viewable data
    const t_byte* tobyte (void) const override;
    
  public:
    /// no lock - set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    void nlset (const t_long pos, const Complex& val) override;

    /// no lock - get a trace value by position
    /// @param pos the trace position
    Complex nlget (const t_long pos) const override;

  public:
    /// create a new object in a generic way
    /// @param argv the argument trace
    static Object* mknew (Vector* argv);

    /// operate this object with another object
    /// @param type   the operator type
    /// @param object the operand object
    Object* oper (t_oper type, Object* object) override;
  };
}

#endif
