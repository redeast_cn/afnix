// ---------------------------------------------------------------------------
// - Cmi.cpp                                                                 -
// - afnix:mth module - complex matrix interface implementation              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cmi.hpp"
#include "Real.hpp"
#include "Math.hpp"
#include "Item.hpp"
#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Utility.hpp"
#include "Algebra.hpp"
#include "Boolean.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "OutputStream.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------
  
  // the default row/column barrier
  static const t_long CMI_ROWB_DEF = 0LL;
  static const t_long CMI_COLB_DEF = 0LL;

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------


  // create a square matrix by size

  Cmi::Cmi (const t_long size) : Ami (size) {
  }

  // create a matrix by size

  Cmi::Cmi (const t_long rsiz, const t_long csiz) : Ami (rsiz, csiz) {
  }

  // copy construct this matrix

  Cmi::Cmi (const Cmi& that) {
    that.rdlock ();
    try {
      Ami::operator = (that);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  
  // copy move this matrix

  Cmi::Cmi (Cmi&& that) noexcept {
    that.wrlock ();
    try {
      Ami::operator = (static_cast<Ami&&>(that));
      that.unlock ();
    } catch (...) {
      d_rsiz = 0LL;
      d_csiz = 0LL;
      that.unlock ();
    } 
  }

  // assign a matrix to this one

  Cmi& Cmi::operator = (const Cmi& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Ami::operator = (that);
      that.unlock ();
      unlock ();
      return *this;
    } catch (...) {
      that.unlock ();
      unlock ();
      throw;
    }
  }
  
  // move a matrix into this one

  Cmi& Cmi::operator = (Cmi&& that) noexcept {
    // check for self-move
    if (this == &that) return *this;
    // lock and move
    wrlock ();
    that.wrlock ();
    try {
      // assign base serial
      Ami::operator = (static_cast<Ami&&>(that));
      unlock ();
      that.unlock ();
    } catch (...) {
      d_rsiz = 0LL;
      d_csiz = 0LL;
      unlock ();
      that.unlock ();
    }
    return *this;
  }
  
  // serialize this object
  
  void Cmi::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      // write the matric row/col size
      Serial::wrlong (d_rsiz, os);
      Serial::wrlong (d_csiz, os);
      // write the matrix data
      for (t_long row = 0; row < d_rsiz; row++) {
	for (t_long col = 0; col < d_csiz; col++) {
	  // get the matrix value
	  Complex val = nlget (row, col);
	  if (val == 0.0) continue;
	  // write by position
	  Serial::wrlong (row, os);
	  Serial::wrlong (col, os);
	  val.wrstream (os);
	}
      }
      // write marker for end
      Serial::wrlong (-1, os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this object

  void Cmi::rdstream (InputStream& is) {
    wrlock ();
    try {
      // get the matrix row/col size
      t_long rsiz = Serial::rdlong (is);
      t_long csiz = Serial::rdlong (is);
      // resize the matrix
      resize (rsiz, csiz);
      // get the matrix data by position
      for (t_long i = 0LL; i < rsiz; i++) {
	for (t_long j = 0; j < csiz; j++) {
	  t_long row = Serial::rdlong (is);
	  // check for marker
	  if (row == -1) {
	    unlock ();
	    return;
	  }
	  t_long col = Serial::rdlong (is);
	  Complex val; val.rdstream (is);
	  nlset (row, col, val);
	}
      }
      // get the remaining marker
      if (Serial::rdlong (is) != -1) {
	throw Exception ("cmi-error", "inconsistent serialized matrix");
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this matrix

  void Cmi::reset (void) {
    wrlock ();
    try {
      d_rsiz = 0LL;
      d_csiz = 0LL;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this matrix

  void Cmi::clear (void) {
    wrlock ();
    try {
      nlclear (0LL, 0LL, d_rsiz, d_csiz);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compare two matrices

  bool Cmi::operator == (const Cmi& x) const {
    rdlock ();
    x.rdlock ();
    try {
      bool result = Algebra::eql (*this, x);
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // compare two matrices

  bool Cmi::operator != (const Cmi& x) const {
    return !(*this == x);
  }
  
  // get the matrix row barrier

  t_long Cmi::getrowb (void) const {
    rdlock ();
    try {
      t_long result = CMI_ROWB_DEF;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the matrix column barrier

  t_long Cmi::getcolb (void) const {
    rdlock ();
    try {
      t_long result = CMI_COLB_DEF;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return true if the matrix is null

  bool Cmi::isnil (void) const {
    rdlock ();
    try {
      bool result = false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear a matrix zone

  void Cmi::clear (const t_long row, const t_long col,
		   const t_long rsz, const t_long csz) {
    wrlock ();
    try {
      nlclear (row, col, rsz, csz);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear the lower triangular part

  void Cmi::clt (void) {
    wrlock ();
    try {
      Algebra::clt (*this);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }	
  }

  // compress the matrix if possible

  bool Cmi::compress (void) {
    wrlock ();
    try {
      bool result = false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set an identity matrix

  void Cmi::eye (void) {
    wrlock ();
    try {
      Algebra::eye (*this);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // create an adjoint matrix

  Cmi* Cmi::adj (void) const {
    rdlock ();
    try {
      // create a zeros matrix
      Cmi* result = zeros ();
      // set the ajoint matrix
      Algebra::adj (*result, *this);
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // copy a matrix diagonal into a trace
  
  Cti* Cmi::cpt (void) const {
    throw Exception ("cmi-error", "unimplemented matrix diagonal copy");
  }

  // copy a matrix row into a vector
  
  Cvi* Cmi::cpr (const t_long row) const {
    throw Exception ("cmi-error", "unimplemented matrix row copy");
  }

  // copy a matrix column into a vector
  
  Cvi* Cmi::cpc (const t_long col) const {
    throw Exception ("cmi-error", "unimplemented matrix column copy");
  }

  // compare two matrices by precision

  bool Cmi::cmp (const Cmi& mx) const {
    rdlock ();
    mx.rdlock ();
    try {
      bool result = Algebra::cmp (*this, mx);
      unlock ();
      mx.unlock ();
      return result;
    } catch (...) {
      unlock ();
      mx.unlock ();
      throw;
    }
  }

  // check if a matrix row is null

  bool Cmi::isnrow (const t_long row) const {
    rdlock ();
    try {
      bool result = Algebra::isnrow (*this, row);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if a matrix column is null

  bool Cmi::isncol (const t_long col) const {
    rdlock ();
    try {
      bool result = Algebra::isncol (*this, col);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compute the matrix frobenius norm
  
  t_real Cmi::norm (void) const {
    rdlock ();
    try {
      t_real result = Algebra::norm (*this);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // fill a matrix with another one - this is the opposite of set

  void Cmi::fill (Cmi& m) const {
    rdlock ();
    m.wrlock ();
    try {
      // reset and resize
      m.reset  ();
      m.resize (getrsiz (), getcsiz ());
      // do a direct copy
      Algebra::cpy (m, *this);
      // done
      unlock ();
      m.unlock ();
    } catch (...) {
      unlock ();
      m.unlock ();
      throw;
    }
  }

  // set a matrix with another one

  void Cmi::set (const Cmi& m) {
    wrlock ();
    m.rdlock ();
    try {
      // reset and resize
      reset  ();
      resize (m.getrsiz (), m.getcsiz ());
      // do a direct copy
      Algebra::cpy (*this, m);
      // done
      unlock ();
      m.unlock ();
    } catch (...) {
      unlock ();
      m.unlock ();
      throw;
    }
  }

  // set a matrix by position

  void Cmi::set (const t_long row, const t_long col, const Complex& val) {
    wrlock ();
    try {
      // check for valid position
      if ((row < 0) || (row >= d_rsiz) ||
	  (col < 0) || (col >= d_csiz)) {
	throw Exception ("index-error", "invalid matrix position in set");
      }
      // set in unlocked mode
      nlset (row, col, val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a matrix value by position

  Complex Cmi::get (const t_long row, const t_long col) const {
    rdlock ();
    try {
      // check for valid position
      if ((row < 0) || (row >= d_rsiz) ||
	  (col < 0) || (col >= d_csiz)) {
	throw Exception ("index-error", "invalid matrix position in get");
      }
      // get in unlocked mode
      Complex result = nlget (row, col);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // multiply a matrix with a vector and a scaling factor

  Cvi& Cmi::mul (Cvi& r, const Cvi& x, const Complex& s) const {
    rdlock ();
    try {
      Cvi& result = nlmul (r, x, s);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // multiply a transposed matrix with a vector and a scaling factor

  Cvi& Cmi::tmul (Cvi& r, const Cvi& x, const Complex& s) const {
    rdlock ();
    try {
      Cvi& result = nltmul (r, x, s);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // permutate this matrix
  
  Cmi* Cmi::permutate (const Cpi& p) const {
    throw Exception ("cmi-error", "unimplemented matrix permutate");
  }

  // reverse permutate this matrix
  
  Cmi* Cmi::reverse (const Cpi& p) const {
    throw Exception ("cmi-error", "unimplemented matrix reverse permutate");
  }

  // -------------------------------------------------------------------------
  // - no lock section                                                       -
  // -------------------------------------------------------------------------
  
  // no lock - clear a matrix zone

  void Cmi::nlclear (const t_long row, const t_long col,
		     const t_long rsz, const t_long csz) {
    // check valid zone
    if ((row < 0LL) || (col < 0LL)) return;
    t_long rsiz = row + rsz; if (rsiz > d_rsiz) rsiz = d_rsiz;
    t_long csiz = col + csz; if (csiz > d_csiz) csiz = d_csiz;
    // clear the zone - might be slow - very slow ...
    for (t_long i = 0LL; i < rsiz; i++) {
      for (t_long j = 0LL; j < csiz; j++) {
	nlset (i, j, 0.0);
      }
    }
  }

  // no lock - multiply a matrix with a vector and a scaling factor

  Cvi& Cmi::nlmul (Cvi& r, const Cvi& x, const Complex& s) const {
    Algebra::mul (r, *this, x, s);
    return r;
  }

  // no lock - multiply a transposed matrix with a vector and a scaling factor

  Cvi& Cmi::nltmul (Cvi& r, const Cvi& x, const Complex& s) const {
    Algebra::tmul (r, *this, x, s);
    return r;
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 19;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_EQL       = zone.intern ("==");
  static const long QUARK_NEQ       = zone.intern ("!=");
  static const long QUARK_QEQ       = zone.intern ("?=");
  static const long QUARK_SET       = zone.intern ("set");
  static const long QUARK_GET       = zone.intern ("get");
  static const long QUARK_EYE       = zone.intern ("eye");
  static const long QUARK_CPT       = zone.intern ("copy-trace");
  static const long QUARK_CPR       = zone.intern ("copy-row");
  static const long QUARK_CPC       = zone.intern ("copy-column");
  static const long QUARK_FILL      = zone.intern ("fill");
  static const long QUARK_NILP      = zone.intern ("nil-p");
  static const long QUARK_NORM      = zone.intern ("norm");
  static const long QUARK_CLLT      = zone.intern ("clear-lower-triangular");
  static const long QUARK_CLEAR     = zone.intern ("clear");
  static const long QUARK_ZEROS     = zone.intern ("zeros");
  static const long QUARK_ADJOINT   = zone.intern ("adjoint");
  static const long QUARK_SQUAREP   = zone.intern ("square-p");
  static const long QUARK_COMPRESS  = zone.intern ("compress");
  static const long QUARK_REVERSE   = zone.intern ("reverse");
  static const long QUARK_PERMUTATE = zone.intern ("permutate");

  // return true if the given quark is defined

  bool Cmi::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true){
	unlock ();
	return true;
      }
      bool result = hflg ? Iterable::isquark (quark, hflg) : false;
      if (result == false) {
	result = hflg ? Ami::isquark (quark, hflg) : false;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // operate this object with another object

  Object* Cmi::oper (t_oper type, Object* object) {
    Cmi* mobj = dynamic_cast <Cmi*> (object);
    switch (type) {
    case Object::OPER_EQL:
      if (mobj != nullptr) return new Boolean (*this == *mobj);
      break;
    case Object::OPER_NEQ:
      if (mobj != nullptr) return new Boolean (*this != *mobj);
      break;
    default:
      break;
    }
    throw Exception ("type-error", "invalid operand with real matrix",
		     Object::repr (object));
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Cmi::apply (Evaluable* zobj, Nameset* nset, const long quark,
                      Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_NORM)     return new Real    (norm ());
      if (quark == QUARK_NILP)     return new Boolean (isnil ());
      if (quark == QUARK_ADJOINT)  return adj ();
      if (quark == QUARK_COMPRESS) return new Boolean (compress ());
      if (quark == QUARK_ZEROS) {
	Cmi* result = zeros ();
	return result;
      }
      if (quark == QUARK_CPT) {
	Cti* result = cpt ();
	return result;
      }
      if (quark == QUARK_EYE) {
	eye ();
	return nullptr;
      }
      if (quark == QUARK_CLLT) {
	clt ();
	return nullptr;
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_EQL) return oper (Object::OPER_EQL, argv->get (0));
      if (quark == QUARK_NEQ) return oper (Object::OPER_NEQ, argv->get (0));
      if (quark == QUARK_CPR) {
	t_long row = argv->getlong (0);
	return cpr (row);
      }
      if (quark == QUARK_CPC) {
	t_long col = argv->getlong (0);
	return cpc (col);
      }
      if (quark == QUARK_QEQ) {
	Object* obj = argv->get (0);
	Cmi*    rmo = dynamic_cast <Cmi*> (obj);
	if (rmo == nullptr) {
	  throw Exception ("type-error", "invalid object for compare",
			   Object::repr (obj));
	}
	return new Boolean (cmp (*rmo));
      }
      if (quark == QUARK_SET) {
	Object* obj = argv->get (0);
	Cmi* m = dynamic_cast <Cmi*> (obj);
	if (m == nullptr) {
	  throw Exception ("type-error", "invalid object with set",
			   Object::repr (obj));
	}
	set (*m);
	return nullptr;
      }
      if (quark == QUARK_FILL) {
	Object* obj = argv->get (0);
	Cmi* m = dynamic_cast <Cmi*> (obj);
	if (m == nullptr) {
	  throw Exception ("type-error", "invalid object with fill",
			   Object::repr (obj));
	}
	fill (*m);
	return nullptr;
      }
      if (quark == QUARK_PERMUTATE) {
	Object* obj = argv->get (0);
	Cpi* p = dynamic_cast <Cpi*> (obj);
	if (p == nullptr) {
	  throw Exception ("type-error", "invalid object with permutate",
			   Object::repr (obj));
	}
	return permutate (*p);
      }
      if (quark == QUARK_REVERSE) {
	Object* obj = argv->get (0);
	Cpi* p = dynamic_cast <Cpi*> (obj);
	if (p == nullptr) {
	  throw Exception ("type-error", "invalid object with permutate",
			   Object::repr (obj));
	}
	return reverse (*p);
      }
    }
    // dispatch 2 argument
    if (argc == 2) {
      if (quark == QUARK_GET) {
        t_long row = argv->getlong (0);
        t_long col = argv->getlong (1);
        return new Complex (get (row, col));
      }
    }
    // dispatch 3 arguments
    if (argc == 3) {
      if (quark == QUARK_SET) {
        t_long row = argv->getlong (0);
        t_long col = argv->getlong (1);
	// collect argument object
	Object* obj = argv->get (2);
	// check for real
	auto robj = dynamic_cast <Real*> (obj);
	if (robj != nullptr) {
	  set (row, col, robj->toreal ());
	  return nullptr;
	}
	// check for complex
	auto cobj = dynamic_cast <Complex*> (obj);
	if (cobj != nullptr) {
	  set (row, col, *cobj);
	  return nullptr;
	}
	throw Exception ("type-error", "invalid object for c-block set",
			 Object::repr (obj));
      }
      // dispatch 4 arguments
      if (argc == 4) {
	if (quark == QUARK_CLEAR) {
	  t_long row = argv->getlong (0);
	  t_long col = argv->getlong (1);
	  t_long rsz = argv->getlong (2);
	  t_long csz = argv->getlong (3);
	  clear (row, col, rsz, csz);
	  return nullptr;
	}
      }
    }
    // check the iterable method
    if (Iterable::isquark (quark, true) == true) {
      return Iterable::apply (zobj, nset, quark, argv);
    }
    // call the ami object
    return Ami::apply (zobj, nset, quark, argv);
  }

  // -------------------------------------------------------------------------
  // - iterator section                                                      -
  // -------------------------------------------------------------------------

  // move the iterator to the next position

  void Cmit::next (void) {
    wrlock ();
    try {
      nlnext ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // move the iterator to the previous position

  void Cmit::prev (void) {
    wrlock ();
    try {
      nlprev ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if the iterator is at the end

  bool Cmit::isend (void) const {
    rdlock ();
    try {
      bool result = nlend ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // move the iterator to a matrix point

  void Cmit::move (const t_long row, const t_long col) {
    wrlock ();
    try {
      nlmove (row, col);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the iterator row coordinate

  t_long Cmit::getrow (void) const {
    rdlock ();
    try {
      t_long result = nlgrow ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the iterator column coordinate
  
  t_long Cmit::getcol (void) const {
    rdlock ();
    try {
      t_long result = nlgcol ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // set the matrix at the iterator position

  void Cmit::setval (const Complex& val) {
    wrlock ();
    try {
      nlsval (val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the iterator matrix value

  Complex Cmit::getval (void) const {
    rdlock ();
    try {
      Complex result = nlgval ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the iterator type

  void Cmit::settype (const t_cmit cmit) {
    wrlock ();
    try {
      // set the iterator type
      d_cmit = cmit;
      // reset to the beginning
      begin ();
      // done
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - iterator object section                                               -
  // -------------------------------------------------------------------------

  // the object eval quarks
  static const long QUARK_CMIT_LENGTH = 2;
  static QuarkZone  cmit (QUARK_CMIT_LENGTH);

  static const long QUARK_CMITSEQ = cmit.intern ("SEQUENTIAL");
  static const long QUARK_CMITROW = cmit.intern ("ROW");
  static const long QUARK_CMITCOL = cmit.intern ("COL");
  static const long QUARK_CMIT    = String::intern ("Cmit");

  // map an enumeration item to a transcoding mode
  static inline Cmit::t_cmit item_to_cmit (const Item& item) {
    // check for a cmit item 
    if (item.gettid () != QUARK_CMIT)
      throw Exception ("item-error", "item is not a cmit item");
    // map the item to the enumeration
    long quark = item.getquark ();
    // map the quark as an iterator mode
    if (quark == QUARK_CMITSEQ) return Cmit::CMIT_SEQ;
    if (quark == QUARK_CMITROW) return Cmit::CMIT_ROW;
    if (quark == QUARK_CMITCOL) return Cmit::CMIT_COL;
    throw Exception ("item-error", "cannot map cmit mode");
  }

  // the quark zone
  static const long QUARK_ZNIT_LENGTH = 2;
  static QuarkZone  znit (QUARK_ZNIT_LENGTH);

  // the object supported quarks
  static const long QUARK_MOVE    = znit.intern ("move");
  static const long QUARK_SETCMIT = znit.intern ("set-type");

  // evaluate a quark statically
  
  Object* Cmit::meval (Evaluable* zobj, Nameset* nset, const long quark) {
    if (cmit.exists (quark) == true) {
      return new Item (QUARK_CMIT, quark);
    }
    throw Exception ("eval-error", "cannot evaluate member",
                     String::qmap (quark));
  }
  
  // return true if the given quark is defined
  
  bool Cmit::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Iterator::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark
  
  Object* Cmit::apply (Evaluable* zobj, Nameset* nset, const long quark,
		       Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETCMIT) {
	Object* iobj = argv->get (0);
        Item*   item = dynamic_cast <Item*> (iobj);
        if (item == nullptr) {       
          throw Exception ("argument-error", "invalid arguments with set-type");
        }
        Cmit::t_cmit cmit = item_to_cmit (*item);
        settype (cmit);
        return nullptr;
      }
    }
    // dispatch 2 arguments
    if (argc == 2) {
      if (quark == QUARK_MOVE) {
	t_long row = argv->getlong (0);
	t_long col = argv->getlong (1);
	move (row, col);
        return nullptr;
      }
    }
    // apply these arguments with the iterator
    return Iterator::apply (zobj, nset, quark, argv);
  }
}
