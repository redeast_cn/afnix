// ---------------------------------------------------------------------------
// - Cti.cpp                                                                 -
// - afnix:mth module - complex trace interface implementation               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cti.hpp"
#include "Math.hpp"
#include "Real.hpp"
#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Utility.hpp"
#include "Algebra.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "OutputStream.hpp"
 
namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a trace by size

  Cti::Cti (const t_long size) : Ati (size) {
  }
  
  // copy construct this trace

  Cti::Cti (const Cti& that) {
    that.rdlock ();
    try {
      Ati::operator = (that);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  
  // copy move this trace

  Cti::Cti (Cti&& that) noexcept {
    that.wrlock ();
    try {
      Ati::operator = (static_cast<Ati&&>(that));
      that.unlock ();
    } catch (...) {
      d_size = 0L;
      that.unlock ();
    }
  }

  // assign a trace to this one

  Cti& Cti::operator = (const Cti& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Ati::operator = (that);
      that.unlock ();
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // move a trace into this one

  Cti& Cti::operator = (Cti&& that) noexcept {
    // check for self-move
    if (this == &that) return *this;
    // lock and move
    wrlock ();
    that.wrlock ();
    try {
      Ati::operator = (static_cast<Ati&&>(that));
      unlock ();
      that.unlock ();
    } catch (...) {
      d_size = 0L;
      unlock ();
      that.unlock ();
    }
    return *this;
  }
  
  // serialize this object
  
  void Cti::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      // write the trace size/mode
      Serial::wrlong (d_size, os);
      // write the trace data
      for (long k = 0; k < d_size; k++) {
	// get the trace value
	Complex val = nlget (k);
	if (val.iszero () == true) continue;
	// write by position
	Serial::wrlong (k,   os);
	val.wrstream (os);
      }
      // write marker for end
      Serial::wrlong (-1, os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this object

  void Cti::rdstream (InputStream& is) {
    wrlock ();
    try {
      // reset the trace
      reset ();
      // get the trace size/mode
      long size = Serial::rdlong (is);
      // resize the trace
      resize (size);
      // get the trace data by position
      for (long k = 0; k < size; k++) {
	long idx = Serial::rdlong (is);
	// check for marker
	if (idx == -1) {
	  unlock ();
	  return;
	}
	Complex val; val.rdstream (is);
	nlset (idx, val);
      }
      // get the remaining marker
      if (Serial::rdlong (is) != -1) {
	throw Exception ("cti-error", "inconsistent serialized trace");
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this trace

  void Cti::reset (void) {
    wrlock ();
    try {
      d_size = 0LL;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this trace

  void Cti::clear (void) {
    wrlock ();
    try {
      for (t_long i = 0; i < d_size; i++) nlset (i, 0.0);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if this trace is nil

  bool Cti::isnil (void) const {
    rdlock ();
    try {
      bool result = Algebra::isnil (*this);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert to a trace of literal

  Vector Cti::tovector (void) const {
    rdlock ();
    try {
      Vector result;
      for (t_long k = 0LL; k < d_size; k++) {
	result.add (new Complex (nlget (k)));
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the trace format

  String Cti::tofrmt (void) const {
    rdlock ();
    try {
      String result = Complex::CV_NUM_FRMT;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // compare two traces

  bool Cti::operator == (const Cti& x) const {
    rdlock ();
    x.rdlock ();
    try {
      bool result = Algebra::eql (*this, x);
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // compare two traces

  bool Cti::operator != (const Cti& x) const {
    return !(*this == x);
  }

  // add this trace by a scalar

  Cti& Cti::operator += (const Complex& s) {
    return this->add (*this, s);
  }

  // add this trace with a trace

  Cti& Cti::operator += (const Cti& x) {
    return this->aeq (x);
  }

  // substract this trace by a scalar

  Cti& Cti::operator -= (const Complex& s) {
    return this->sub (*this, s);
  }

  // multiply this trace by a scalar

  Cti& Cti::operator *= (const Complex& s) {
    return this->mul (*this, s);
  }
  
  // copy a trace into this one

  Cti& Cti::cpy (const Cti& x) {
    wrlock ();
    try {
      Algebra::cpy (*this, x);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compare a trace value by position

  bool Cti::cmp (const t_long pos, const Complex& val) const {
    rdlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid trace position");
      }
      Complex z = nlget (pos);
      bool result = z.cmp (val);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compare two traces by precision

  bool Cti::cmp (const Cti& x) const {
    rdlock ();
    x.rdlock ();
    try {
      bool result = Algebra::cmp (*this, x);
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // set an identity trace

  void Cti::eye (void) {
    wrlock ();
    try {
      Algebra::eye (*this);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // set a trace by value

  void Cti::set (const Complex& val) {
    wrlock ();
    try {
      for (t_long i = 0; i < d_size; i++) nlset (i, val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set a trace by position

  void Cti::set (const t_long pos, const Complex& val) {
    wrlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid trace position");
      }
      nlset (pos, val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a trace by position

  Complex Cti::get (const t_long pos) const {
    rdlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid trace position");
      }
      Complex result = nlget (pos);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a trace with a scalar

  Cti& Cti::add (const Cti& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::add (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a trace with another one

  Cti& Cti::add (const Cti& x, const Cti& y) {
    wrlock ();
    try {
      Algebra::add (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a trace with another scaled one

  Cti& Cti::add (const Cti& x, const Cti& y, const Complex& s) {
    wrlock ();
    try {
      Algebra::add (*this, x, y, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // substract a trace with a scalar

  Cti& Cti::sub (const Cti& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::sub (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // substract a trace with another one

  Cti& Cti::sub (const Cti& x, const Cti& y) {
    wrlock ();
    try {
      Algebra::sub (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // multiply a trace with a scaled factor

  Cti& Cti::mul (const Cti& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::mul (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // multiply a trace with another one

  Cti& Cti::mul (const Cti& x, const Cti& y) {
    wrlock ();
    try {
      Algebra::mul (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // divide a trace with another one

  Cti& Cti::div (const Cti& x, const Cti& y) {
    wrlock ();
    try {
      Algebra::div (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add equal with a trace

  Cti& Cti::aeq (const Cti& x) {
    wrlock ();
    try {
      Algebra::aeq (*this, x);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add equal with a scaled trace

  Cti& Cti::aeq (const Cti& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::aeq (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // rescale equal with a trace

  Cti& Cti::req (const Cti& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::req (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // permutate this trace
  
  Cti* Cti::permutate (const Cpi& p) const {
    throw Exception ("cti-error", "unimplemented trace permutate");
  }

  // reverse permutate this trace
  
  Cti* Cti::reverse (const Cpi& p) const {
    throw Exception ("cti-error", "unimplemented trace reverse permutate");
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 12;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_EQL       = zone.intern ("==");
  static const long QUARK_NEQ       = zone.intern ("!=");
  static const long QUARK_QEQ       = zone.intern ("?=");
  static const long QUARK_AEQ       = zone.intern ("+=");
  static const long QUARK_SEQ       = zone.intern ("-=");
  static const long QUARK_MEQ       = zone.intern ("*=");
  static const long QUARK_DEQ       = zone.intern ("/=");
  static const long QUARK_EYE       = zone.intern ("eye");
  static const long QUARK_SET       = zone.intern ("set");
  static const long QUARK_GET       = zone.intern ("get");
  static const long QUARK_REVERSE   = zone.intern ("reverse");
  static const long QUARK_PERMUTATE = zone.intern ("permutate");

  // return true if the given quark is defined

  bool Cti::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true){
	unlock ();
	return true;
      }
      bool result = hflg ? Ati::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // operate this object with another object

  Object* Cti::oper (t_oper type, Object* object) {
    Cti* vobj = dynamic_cast <Cti*> (object);
    switch (type) {
    case Object::OPER_EQL:
      if (vobj != nullptr) return new Boolean (*this == *vobj);
      break;
    case Object::OPER_QEQ:
      if (vobj != nullptr) return new Boolean (cmp(*vobj));
      break;
    case Object::OPER_NEQ:
      if (vobj != nullptr) return new Boolean (*this != *vobj);
      break;
    default:
      break;
    }
    throw Exception ("type-error", "invalid operand with real trace",
		     Object::repr (object));
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Cti::apply (Evaluable* zobj, Nameset* nset, const long quark,
                      Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_EYE) {
	eye ();
        return nullptr;
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_EQL) return oper (Object::OPER_EQL, argv->get (0));
      if (quark == QUARK_QEQ) return oper (Object::OPER_QEQ, argv->get (0));
      if (quark == QUARK_NEQ) return oper (Object::OPER_NEQ, argv->get (0));
      if (quark == QUARK_GET) {
        t_long pos = argv->getlong (0);
        return new Complex (get (pos));
      }
      if (quark == QUARK_AEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  // check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this += Complex(robj->toreal ());
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this += *cobj;
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a trace
	  auto vobj = dynamic_cast <Cti*> (obj);
	  if (vobj != nullptr) {
	    *this += (*vobj);
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for trace aeq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_SEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  //  check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this -= robj->toreal ();
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  //  check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this -= *cobj;
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for trace seq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_MEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  // check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this *= robj->toreal ();
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this *= *cobj;
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for trace meq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_DEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  // check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this *= (1.0 / robj->toreal());
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this *= (1.0 / *cobj);
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for trace deq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_PERMUTATE) {
	Object* obj = argv->get (0);
	Cpi* p = dynamic_cast <Cpi*> (obj);
	if (p == nullptr) {
	  throw Exception ("type-error", "invalid object with permutate",
			   Object::repr (obj));
	}
	return permutate (*p);
      }
      if (quark == QUARK_REVERSE) {
	Object* obj = argv->get (0);
	Cpi* p = dynamic_cast <Cpi*> (obj);
	if (p == nullptr) {
	  throw Exception ("type-error", "invalid object with permutate",
			   Object::repr (obj));
	}
	return reverse (*p);
      }
    }
    // dispatch 2 arguments
    if (argc == 2) {
      if (quark == QUARK_SET) {
        t_long pos = argv->getlong (0);
	Object* obj = argv->get (1);
	// check for real
	auto robj = dynamic_cast <Real*> (obj);
	if (robj != nullptr) {
	  set (pos, robj->toreal ());
	  return nullptr;
	}
	// check for complex
	auto cobj = dynamic_cast <Complex*> (obj);
	if (cobj != nullptr) {
	  set (pos, *cobj);
	  return nullptr;
	}
	throw Exception ("type-error", "invalid object for trace set",
			 Object::repr (obj));
      }
      if (quark == QUARK_QEQ) {
	t_long  pos = argv->getlong (0);
	Object* obj = argv->get (1);
	// check for a real
	auto robj = dynamic_cast <Real*> (obj);
	if (robj != nullptr) {
	  return new Boolean (cmp (pos, robj->toreal()));
	}
	// check for a complex
	auto cobj = dynamic_cast <Complex*> (obj);
	if (cobj != nullptr) {
	  return new Boolean (cmp (pos, *cobj));
	}
	throw Exception ("type-error", "invalid object for trace qeq",
			 Object::repr (obj));
      }
    }
    // call the abstract trace
    return Ati::apply (zobj, nset, quark, argv);
  }
}

