// ---------------------------------------------------------------------------
// - Ctrace.cpp                                                              -
// - afnix:mth module - complex trace implementation                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Real.hpp"
#include "Math.hpp"
#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Ctrace.hpp"
#include "Algebra.hpp"
#include "Utility.hpp"
#include "Exception.hpp"
 
namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // add a trace with a scalar

  Ctrace operator + (const Ctrace& x, const Complex& s) {
    x.rdlock ();
    try {
      // create a result trace
      Ctrace r (x.getsize ());
      // add the scalar
      r.add (x, s);
      // unlock and return
      x.unlock ();
      return r;
    } catch (...) {
      x.unlock ();
      throw;
    }
  }

  // add a trace with another one

  Ctrace operator + (const Ctrace& x, const Ctrace& y) {
    x.rdlock ();
    y.rdlock ();
    try {
      // create a result trace
      Ctrace r (x.getsize ());
      // add the scalar
      r.add (x, y);
      // unlock and return
      x.unlock ();
      y.unlock ();
      return r;
    } catch (...) {
      x.unlock ();
      y.unlock ();
      throw;
    }
  }

  // substract a trace with a scalar

  Ctrace operator - (const Ctrace& x, const Complex& s) {
    x.rdlock ();
    try {
      // create a result trace
      Ctrace r (x.getsize ());
      // add the scalar
      r.sub (x, s);
      // unlock and return
      x.unlock ();
      return r;
    } catch (...) {
      x.unlock ();
      throw;
    }
  }

  // substract a trace with another one

  Ctrace operator - (const Ctrace& x, const Ctrace& y) {
    x.rdlock ();
    y.rdlock ();
    try {
      // create a result trace
      Ctrace r (x.getsize ());
      // add the scalar
      r.sub (x, y);
      // unlock and return
      x.unlock ();
      y.unlock ();
      return r;
    } catch (...) {
      x.unlock ();
      y.unlock ();
      throw;
    }
  }

  // multiply a trace with a scalar

  Ctrace operator * (const Ctrace& x, const Complex& s) {
    x.rdlock ();
    try {
      // create a result trace
      Ctrace r (x.getsize ());
      // add the scalar
      r.mul (x, s);
      // unlock and return
      x.unlock ();
      return r;
    } catch (...) {
      x.unlock ();
      throw;
    }
  }

  // divide a trace with a scalar

  Ctrace operator / (const Ctrace& x, const Complex& s) {
    x.rdlock ();
    try {
      // create a result trace
      Ctrace r (x.getsize ());
      // add the scalar
      r.mul (x, Complex(1.0, 0.0) / s);
      // unlock and return
      x.unlock ();
      return r;
    } catch (...) {
      x.unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default trace

  Ctrace::Ctrace (void) {
    p_vtab = nullptr;
  }

  // create a trace by size

  Ctrace::Ctrace (const t_long size) : Cti (size) {
    p_vtab = nullptr;
    preset ();
  }
  
  // copy construct this trace

  Ctrace::Ctrace (const Ctrace& that) {
    that.rdlock ();
    try {
      // copy base class
      Cti::operator = (that);
      // copy locally
      p_vtab = nullptr;
      if (d_size > 0) {
	long zsiz = 2 * d_size; p_vtab = new t_real[zsiz];
	for (t_long i = 0; i < zsiz; i++) p_vtab[i] = that.p_vtab[i];
      }
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
	
  // destroy this trace

  Ctrace::~Ctrace (void) {
    delete [] p_vtab;
  }

  // assign a trace to this one

  Ctrace& Ctrace::operator = (const Ctrace& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // clean locally
      delete [] p_vtab; p_vtab = nullptr;
      // assign base class
      Cti::operator = (that);
      // assign locally
      if (d_size > 0) {
	long zsiz = 2 * d_size; p_vtab = new t_real[zsiz];
	for (t_long i = 0; i < zsiz; i++) p_vtab[i] = that.p_vtab[i];
      }
      // unlock and return
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // compare two traces

  bool Ctrace::operator == (const Cti& x) const {
    rdlock ();
    x.rdlock ();
    try {
      // check size first
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", 
			 "incompatible size with operator ==");
      }
      // initialize result
      bool result = true;
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	Complex ti = nlget (i);
	Complex xi = x.nlget (i);
	if (ti != xi) {
	  result = false;
	  break;
	}
      }
      // unlock and return
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // return the class name

  String Ctrace::repr (void) const {
    return "Ctrace";
  }

  // return a clone of this object

  Object* Ctrace::clone (void) const {
    return new Ctrace (*this);
  }
  
  // return the serial did

  t_word Ctrace::getdid (void) const {
    return SRL_DEOD_MTH;
  }

  // return the serial sid

  t_word Ctrace::getsid (void) const {
    return SRL_CTRC_SID;
  }
  
  // resize this trace

  void Ctrace::resize (const t_long size) {
    wrlock ();
    try {
      if (size < 0) {
	throw Exception ("ctrace-error", "invalid negatize size in resize");
      }
      if (size == 0)  {
	delete [] p_vtab;
	d_size = 0;
	p_vtab = nullptr;
	unlock ();
	return;
      }
      // do nothing if equal
      if (size == d_size) {
	unlock ();
	return;
      }
      // create a new array by size
      long zsiz = 2 * size; t_real* vtab = new t_real[zsiz];
      // check for smaller size
      if (size < d_size) {
	for (long k = 0; k < zsiz; k++) {
	  vtab[k] = (p_vtab == nullptr) ? 0.0 : p_vtab[k];
	}
      } else {
	for (long k = 0; k < zsiz; k++) {
	  vtab[k] = (p_vtab == nullptr) ? 0.0 : p_vtab[k];
	}
	for (long k = 2*d_size; k < zsiz; k++) vtab[k] = 0.0;
      }
      d_size = size;
      delete [] p_vtab;
      p_vtab = vtab;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this trace

  void Ctrace::reset (void) {
    wrlock ();
    try {
      delete [] p_vtab;
      d_size = 0;
      p_vtab = nullptr;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this trace

  void Ctrace::clear (void) {
    wrlock ();
    try {
      for (t_long k = 0; k < d_size; k++) {
	p_vtab[k] = 0.0;
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // preset this trace

  void Ctrace::preset (void) {
    wrlock ();
    try {
      delete [] p_vtab;
      p_vtab = (d_size == 0) ? nullptr : new t_real[2*d_size];
      clear ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // copy a trace into this one

  Cti& Ctrace::cpy (const Cti& x) {
    wrlock ();
    x.rdlock ();
    try {
      // check target size
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", "incompatible size in trace copy");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) nlset (i, x.nlget (i));
      unlock ();
      x.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // add a trace with a scalar

  Cti& Ctrace::add (const Cti& x, const Complex& s) {
    wrlock ();
    x.rdlock ();
    try {
      // check target size
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", "incompatible size in trace add");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) nlset (i, x.nlget (i) + s);
      unlock ();
      x.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // add a trace with another one

  Cti& Ctrace::add (const Cti& x, const Cti& y) {
    wrlock ();
    x.rdlock ();
    y.rdlock ();
    try {
      // check target size
      if ((d_size != x.getsize ()) || (d_size != y.getsize ())) {
	throw Exception ("trace-error", "incompatible size in trace add");
      }
      for (t_long i = 0; i < d_size; i++) {
	nlset (i, x.nlget (i) + y.nlget (i));
      }
      unlock ();
      x.unlock ();
      y.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      y.unlock ();
      throw;
    }
  }

  // add a trace with another scaled one

  Cti& Ctrace::add (const Cti& x, const Cti& y, const Complex& s) {
    wrlock ();
    x.rdlock ();
    y.rdlock ();
    try {
      // check target size
      if ((d_size != x.getsize ()) || (d_size != y.getsize ())) {
	throw Exception ("trace-error", "incompatible size in trace add");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	nlset (i,  x.nlget (i) + (s * y.nlget (i)));
      }
      unlock ();
      x.unlock ();
      y.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      y.unlock ();
      throw;
    }
  }

  // substract a trace with a scalar

  Cti& Ctrace::sub (const Cti& x, const Complex& s) {
    wrlock ();
    x.rdlock ();
    try {
      // check target size
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", "incompatible size in trace sub");
      }
      for (t_long i = 0; i < d_size; i++) nlset (i, x.nlget (i) - s);
      unlock ();
      x.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // substract a trace with another one

  Cti& Ctrace::sub (const Cti& x, const Cti& y) {
    wrlock ();
    x.rdlock ();
    y.rdlock ();
    try {
      // check target size
      if ((d_size != x.getsize ()) || (d_size != y.getsize ())) {
	throw Exception ("trace-error", "incompatible size in trace sub");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	nlset (i, x.nlget (i) - y.nlget (i));
      }
      unlock ();
      x.unlock ();
      y.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      y.unlock ();
      throw;
    }
  }

  // multiply a trace with a scalar

  Cti& Ctrace::mul (const Cti& x, const Complex& s) {
    wrlock ();
    x.rdlock ();
    try {
      // check target size
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", "incompatible size in trace mul");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) nlset (i, s * x.nlget (i));
      unlock ();
      x.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // multiply a trace with another one

  Cti& Ctrace::mul (const Cti& x, const Cti& y) {
    wrlock ();
    x.rdlock ();
    y.rdlock ();
    try {
      // check target size
      if ((d_size != x.getsize ()) || (d_size != y.getsize ())) {
	throw Exception ("trace-error", "incompatible size in trace mul");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	nlset (i , x.nlget (i) * y.nlget (i));
      }
      unlock ();
      x.unlock ();
      y.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      y.unlock ();
      throw;
    }
  }

  // divide a trace with another one

  Cti& Ctrace::div (const Cti& x, const Cti& y) {
    wrlock ();
    x.rdlock ();
    y.rdlock ();
    try {
      // check target size
      if ((d_size != x.getsize ()) || (d_size != y.getsize ())) {
	throw Exception ("trace-error", "incompatible size in trace div");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	nlset (i, x.nlget (i) / y.nlget (i));
      }
      unlock ();
      x.unlock ();
      y.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      y.unlock ();
      throw;
    }
  }

  // add a trace with another one

  Cti& Ctrace::aeq (const Cti& x) {
    wrlock ();
    x.rdlock ();
    try {
      // check target size
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", "incompatible size in trace aeq");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	nlset (i, nlget (i) + x.nlget (i));
      }
      unlock ();
      x.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // add a trace with a scaled trace

  Cti& Ctrace::aeq (const Cti& x, const Complex& s) {
    wrlock ();
    x.rdlock ();
    try {
      // check target size
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", "incompatible size in trace aeq");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	nlset (i, nlget (i) + (s * x.nlget (i)));
      }
      unlock ();
      x.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // rescale equal with a trace

  Cti& Ctrace::req (const Cti& x, const Complex& s) {
    wrlock ();
    x.rdlock ();
    try {
      // check target size
      if (d_size != x.getsize ()) {
	throw Exception ("trace-error", "incompatible size in trace req");
      }
      // loop in locked mode
      for (t_long i = 0; i < d_size; i++) {
	nlset (i, (s * nlget (i)) + x.nlget (i));
      }
      unlock ();
      x.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // permutate this trace

  Cti* Ctrace::permutate (const Cpi& p) const {
    rdlock ();
    Cti* result = nullptr;
    try {
      // create a result trace
      result = new Ctrace (d_size);
      // permutate this trace
      Algebra::permutate (*result, *this, p);
      unlock ();
      return result;
    } catch (...) {
      delete result;
      unlock ();
      throw;
    }
  }

  // reverse permutate this trace

  Cti* Ctrace::reverse (const Cpi& p) const {
    rdlock ();
    Cti* result = nullptr;
    try {
      // create a result trace
      result = new Ctrace (d_size);
      // permutate this trace
      Algebra::reverse (*result, *this, p);
      unlock ();
      return result;
    } catch (...) {
      delete result;
      unlock ();
      throw;
    }
  }

  // get the viewable size

  long Ctrace::tosize (void) const {
    rdlock ();
    try {
      long result = 2 * d_size * sizeof(t_real);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the viewable data

  t_byte* Ctrace::tobyte (void) {
    wrlock ();
    try {
      auto result = reinterpret_cast<t_byte*>(p_vtab);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the viewable data

  const t_byte* Ctrace::tobyte (void) const {
    rdlock ();
    try {
      auto result = reinterpret_cast<const t_byte*>(p_vtab);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - no lock section                                                        -
  // -------------------------------------------------------------------------

  // no lock - set a trace by position

  void Ctrace::nlset (const t_long pos, const Complex& val) {
    t_long idx = 2 * pos;
    auto  zval = reinterpret_cast<const t_real*>(val.tobyte());
    if (p_vtab != nullptr) {
      p_vtab[idx]   = zval[0];
      p_vtab[idx+1] = zval[1];
    }
  }

  // no lock - get a trace by position
  
  Complex Ctrace::nlget (const t_long pos) const {
    if (p_vtab == nullptr) return 0.0;
    Complex result (p_vtab[2*pos], p_vtab[2*pos+1]);
    return result;
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way

  Object* Ctrace::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) return new Ctrace;
    // check for 1 argument
    if (argc == 1) {
      t_long size = argv->getlong (0);
      return new Ctrace (size);
    }
    // invalid arguments
    throw Exception ("argument-error", 
		     "invalid arguments with real trace object");
  }

  // operate this trace with another object

  Object* Ctrace::oper (t_oper type, Object* object) {
    Real*    robj = dynamic_cast <Real*>    (object);
    Complex* cobj = dynamic_cast <Complex*> (object);
    Ctrace* vobj = dynamic_cast <Ctrace*> (object);
    switch (type) {
    case Object::OPER_ADD:
      if (vobj != nullptr) return new Ctrace (*this + *vobj);
      if (cobj != nullptr) return new Ctrace (*this + *cobj);
      if (robj != nullptr) return new Ctrace (*this +
					      Complex (robj->toreal()));
      break;
    case Object::OPER_SUB:
      if (vobj != nullptr) return new Ctrace (*this - *vobj);
      if (cobj != nullptr) return new Ctrace (*this - *cobj);
      if (robj != nullptr) return new Ctrace (*this -
					      Complex (robj->toreal()));
      break;
    case Object::OPER_MUL:
      if (cobj != nullptr) return new Ctrace (*this * *cobj);
      if (robj != nullptr) return new Ctrace (*this *
					      Complex (robj->toreal()));
      break;
    case Object::OPER_DIV:
      if (cobj != nullptr) return new Ctrace (*this / *cobj);
      if (robj != nullptr) return new Ctrace (*this /
					      Complex (robj->toreal()));
      break;
    default:
      break;
    }
    // call the cti operator
    return Cti::oper (type, object);
  }
}

