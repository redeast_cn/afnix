// ---------------------------------------------------------------------------
// - Cubmap.hpp                                                              -
// - afnix:dip service - cube pixmap class definition                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CUBMAP_HPP
#define  AFNIX_CUBMAP_HPP

#ifndef  AFNIX_PIXMAP_HPP
#include "Pixmap.hpp"
#endif

namespace afnix {
  
  /// The Cubmap class is a cube oriented pixmap representation. The cubemap
  /// is organized by face with various standard format. When a cubemap is
  /// created from a pixmap, the original pixmap is moved to the cubmap.
  /// @author amaury darsch

  class Cubmap : public Pixmap {
  public:
    /// the cubmap face
    enum t_face : long
      {
	FACE_POSX = 0L, // +X
	FACE_NEGX = 1L, // -X
	FACE_POSY = 2L, // +Y
	FACE_NEGY = 3L, // -Y
	FACE_POSZ = 4L, // +Z
	FACE_NEGZ = 5L  // -Z
      };
    
  public:
    /// create a default cubmap
    Cubmap (void) =default;

    /// create a cubmap by pixmap
    /// @param pixm the pixmap image
    Cubmap (Pixmap* pixm);

    /// create a cubmap by pixmap
    /// @param pixm the pixmap image
    Cubmap (const Pixmap& pixm);

    /// copy construct this cubmap
    /// @param that the cubmap to copy
    Cubmap (const Cubmap& that);

    /// copy move this cubmap
    /// @param that the cubmap to move
    Cubmap (Cubmap&& that) noexcept;

    /// assign a cubmap to this one
    /// @param that the cubmap to assign
    Cubmap& operator = (const Cubmap& that);

    /// move an cubmap to this one
    /// @param that the cubmap to move
    Cubmap& operator = (Cubmap&& that) noexcept;

    /// @return the class name
    String repr (void) const override;
    
    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;

    /// serialize this cubmap
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this cubmap
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// @return the face normalized left x position
    virtual t_real getfnlx (const t_face face) const;

    /// @return the face normalized right x position
    virtual t_real getfnrx (const t_face face) const;
    
    /// @return the face normalized top y position
    virtual t_real getfnty (const t_face face) const;

    /// @return the face normalized bottom y position
    virtual t_real getfnby (const t_face face) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
  };
}

#endif
