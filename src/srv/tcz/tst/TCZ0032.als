# ---------------------------------------------------------------------------
# - TCZ0032.als                                                             -
# - afnix:tcz service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   mixture test unit
# @author amaury darsch

# get the service
interp:library "afnix-tcz"

# create a nil mixture
trans mixt (afnix:tcz:Mixture)

# check predicate and representation
assert true   (afnix:tcz:mixture-p mixt)
assert "Mixture" (mixt:repr)

# check content
assert 0 (mixt:length)
assert afnix:tcz:Tcz:CONTENT-MODE-NONE (mixt:get-content-mode)

# create a datum
const dtum (afnix:tcz:Datum "datum" "datum part")
assert true (dtum:bind afnix:tcz:Tcz:CONTENT-MODE-CONSTANT 0)

# check mixture mode
mixt:set-content-mode afnix:tcz:Tcz:CONTENT-MODE-INPUT
assert afnix:tcz:Tcz:CONTENT-MODE-INPUT (mixt:get-content-mode)

# add and check content
mixt:add dtum
assert 1 (mixt:length)

# get and check
trans mdtm  (mixt:get 0)
assert true (afnix:tcz:datum-p mdtm)
assert 0    (mdtm:get-value)
assert "datum" (mdtm:get-name)

# find and check
trans mdtm  (mixt:find "datum")
assert true (afnix:tcz:datum-p mdtm)
assert 0    (mdtm:get-value)
assert "datum" (mdtm:get-name)
