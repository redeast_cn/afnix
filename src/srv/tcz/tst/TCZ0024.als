# ---------------------------------------------------------------------------
# - TCZ0024.als                                                             -
# - afnix:tcz service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   visa test unit
# @author amaury darsch

# get the service
interp:library "afnix-tcz"

# create an afnix authority
const anam "afnix"
const anfo "afnix authority"
trans auth (afnix:tcz:Authority anam anfo)

# create a wanix identity
const wnam "wanix"
const wnfo "wanix identity"
trans wdty (afnix:tcz:Identity wnam wnfo)

# create a guest identity
const inam "guest"
const info "guest identity"
trans idty (afnix:tcz:Identity inam info)

# create a visa
const visa (afnix:tcz:Visa auth wdty idty)

# check predicate and representation
assert true  (afnix:tcz:act-p visa)
assert true  (afnix:tcz:visa-p visa)
assert "Visa" (visa:repr)
