# ---------------------------------------------------------------------------
# - TCZ0031.als                                                             -
# - afnix:tcz service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   datum test unit
# @author amaury darsch

# get the service
interp:library "afnix-tcz"

# create a nil datum
trans dtum (afnix:tcz:Datum)

# check predicate and representation
assert true   (afnix:tcz:datum-p dtum)
assert "Datum" (dtum:repr)

# check mode
assert afnix:tcz:Tcz:CONTENT-MODE-NONE (dtum:get-content-mode)

# bind and check
assert true (dtum:bind afnix:tcz:Tcz:CONTENT-MODE-CONSTANT 1)
assert afnix:tcz:Tcz:CONTENT-MODE-CONSTANT (dtum:get-content-mode)
assert 1 (dtum:get-value)

