# ---------------------------------------------------------------------------
# - TCZ0025.als                                                             -
# - afnix:tcz service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   act test unit
# @author amaury darsch

# get the service
interp:library "afnix-tcz"

# create an afnix notary
const name "afnix"
const info "afnix notary"
trans ntry (afnix:tcz:Notary name info)

# check predicate and representation
assert true  (afnix:tcz:nameable-p ntry)
assert true  (afnix:tcz:notary-p ntry)
assert "Notary" (ntry:repr)
