// ---------------------------------------------------------------------------
// - Tcz.hpp                                                                 -
// - afnix:tcz service - tcz class definition                                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TCZ_HPP
#define  AFNIX_TCZ_HPP

#ifndef  AFNIX_ITEM_HPP
#include "Item.hpp"
#endif

namespace afnix {

  /// The Tcz class defines the common types associated with the transmutable
  /// content zone. In particular, it defines the content mode which is used
  /// by the datum and mixture.
  /// @author amaury darsch

  class Tcz {
  public:
    /// the content mode
    enum t_cmod : t_byte {
      CMOD_NONE = 0x00U, // nil
      CMOD_CNST = 0x01U, // constant
      CMOD_IPUT = 0x02U, // input
      CMOD_OPUT = 0x03U  // output
    };

    /// map an item into a content mode
    /// @param item the item to map
    static t_cmod tocmod (const Item& item);

    /// map a string item into a content mode
    /// @param smod the string content mode
    static t_cmod tocmod (const String& smod);

    /// map a content mode into an item
    /// @param cmod the content mode to map
    static Item* toitem (const t_cmod cmod);

    /// map a content mode into a string
    /// @param cmod the content mode to map
    static String tostring (const t_cmod cmod);

  public:
    /// evaluate an object data member
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset
    /// @param quark the quark to evaluate
    static Object* meval (Evaluable* zobj, Nameset* nset, const long quark);
  };
}

#endif
