// ---------------------------------------------------------------------------
// - Transmuter.hpp                                                          -
// - afnix:tcz service - mixture transmutation class definition                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TRANSMUTER_HPP
#define  AFNIX_TRANSMUTER_HPP

#ifndef  AFNIX_HYPERP_HPP
#include "Hyperp.hpp"
#endif

#ifndef  AFNIX_TRANSMUTABLE_HPP
#include "Transmutable.hpp"
#endif

namespace afnix {

  /// The Transmuter class is a mixture transformation that implements the
  /// transmutable interface. The transmuter operates with an hyper interpreter
  /// which is configured during the staging process.
  /// @author amaury darsch

  class Transmuter : public Transmutable {
  protected:
    /// the transmuter parameters
    Plist d_plst;
    /// the kernel module
    Module* p_kern;
    /// the hyper interpreter
    Hyperp* p_hpri;
    
  public:
    /// create a default transmuter
    Transmuter (void);

    /// create a transmuter by module
    /// @param kern the kernel module
    Transmuter (Module* kern);

    /// destroythis transmuter
    ~Transmuter (void);

    /// @return the class name
    String repr (void) const override;

    /// reset this transmuter
    void reset (void) override;

    /// stage this transmuter by micture
    bool stage (void) override;

    /// process with a logger
    /// @param logr the logger object
    bool process (Logger* logr) override;

    /// bind a mixture
    /// @param mixt the mixture to bind
    bool bind (Mixture* mixt) override;

    /// bind a kernel module
    /// @param kern the module to bind
    virtual bool bind (Module* kern);

  private:
    /// make the copy conructor private
    Transmuter (const Transmuter&) =delete;
    /// make the assignation operator private
    Transmuter& operator = (const Transmuter&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
	/// @param quark the quark to apply these arguments
	/// @param argv  the arguments  to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
