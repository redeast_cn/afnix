// ---------------------------------------------------------------------------
// - TlsDhe.hpp                                                              -
// - afnix:tls service - tls dhe parameters class definition                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSDHE_HPP
#define  AFNIX_TLSDHE_HPP

#ifndef  AFNIX_KEY_HPP
#include "Key.hpp"
#endif

#ifndef  AFNIX_TLSHBLOCK_HPP
#include "TlsHblock.hpp"
#endif

namespace afnix {

  /// The TlsDhe class is the tls dh(e) parameters class. The class can be
  /// derived from a handshake message by decoding or can also be
  /// constructed from a dh parameter file.
  /// @author amaury darsch

  class TlsDhe : public TlsInfos {
  private:
    /// the dhe parameter name
    String d_dhep;
    /// the dhe key
    Key* p_dhek;
    
  public:
    /// create an empty dhe parameters
    TlsDhe (void);

    /// create a dhe parameters by path
    /// @param path the parameter file path
    TlsDhe (const String& path);

    /// create a dhe parameters by handshake block
    /// @aparam hblk the handshake block
    TlsDhe (TlsHblock* hblk);

    /// destroy this dhe parameters
    ~TlsDhe (void);
    
    /// @return the class name
    String repr (void) const;

    /// reset this block
    void reset (void);

    /// @return the block info as a plist
    Plist getinfo (void) const;

    /// set the dhe parameter path
    /// @param path the parameter path
    virtual void setdhep (const String& path);
    
    /// get the dhe private key
    virtual Key* getprvk (void) const;

    /// get the dhe public key
    virtual Key* getpubk (void) const;
    
    /// decode a handshake block
    /// @param hblk the block to decode
    virtual void decode (TlsHblock* hblk);
    
    /// @return the dhe parameters as a chunk
    virtual TlsChunk tochunk (void) const;

    /// @return the common key exchange
    virtual Relatif tokxch (const Relatif& ykey) const;
    
  private:
    // make the copy constructor private
    TlsDhe (const TlsDhe&);
    // make the assignment operator private
    TlsDhe& operator = (const TlsDhe&);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
